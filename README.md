hdcpharma
=========

* Install composer

<pre>curl -sS https://getcomposer.org/installer | php

sudo mv composer.phar /usr/local/bin/composer
</pre>


* Install PHP-CURL

<pre>sudo apt-get install php5-curl</pre>


* Install FosUserBundle

<pre>composer require friendsofsymfony/user-bundle "~2.0@dev"</pre>


* Update Vendors
<pre>composer update</pre>


* Generate Entities getters and setters
<pre>php app/console doctrine:generate:entities AppBundle</pre>


* Add adminuser

<pre>php app/console fos:user:create adminuser --super-admin</pre>

* Check valid and failed lines for Prospect Import file
<pre>php app/console prospect:check path/to/filename.csv</pre>

* Check and save valid and failed lines for Prospect Import file
<pre>php app/console prospect:check path/to/filename.csv --force</pre>