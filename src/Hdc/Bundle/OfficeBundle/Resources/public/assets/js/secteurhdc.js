$(document).ready(function() {


    $(".select_filtres").change(function(){
        var options = $(this);
        options.
            find('option:selected').
            each(function(){
                if ($(this).val()==0) {
                    options.find('option').prop('selected', true);
                    $(this).removeAttr('selected');
                }
            });

    });


    // get gouvernorats by region
    $("#select_Region").change(function(){
        var items = [];
        $(this).find('option:selected').each(function(){ items.push($(this).val()); });

        $.ajax({
            url: "/gouvernorat/getgouvernorat_by_region/",
            data: {region_id : items},
            dataType: 'json',
            type: 'POST'
        }).done(function(data, status) {
                if ( status === "success") {
                    var text = "<option value='0'>Tous</option>";
                    $.each(data, function (i, row) {
                        text += "<option value='"+row.id+"'>"+row.name+"</option>";
                    });
                    $("#select_Gouvernorat").html(text);
                    getProspectCount();
                } else {
                    alert("Une erreur s'est produite");
                }
            });
    });

    // get brickims by gouvernorats
    $("#select_Gouvernorat").change(function(){
        var items = [];
        $(this).find('option:selected').each(function(){ items.push($(this).val()); });
        var items_specialite = [];
        checkSelectorValue("#select_Specialite", items_specialite);

        $.ajax({
            url: "/brickims/getbrick_by_gouvernorat/",
            data: {gouv_id : items, specialite_id:items_specialite},
            dataType: 'json',
            type: 'POST'
        }).done(function(data, status) {
                if ( status === "success") {
                    var text = "<option value='0'>Tous</option>";
                    $.each(data, function (i, row) {
                        text += "<option value='"+row.id+"'>"+row.name+"</option>";
                    });
                    $("#select_BrickIMS").html(text);
                    if(data.length == 0)
                        $("#select_Ville").html(text);
                    getProspectCount();
                } else {
                    alert("Une erreur s'est produite");
                }
            });
    });


    // get available ville by brickims
    $("#select_BrickIMS").change(function(){
        var items = [];
        var items_specialite = [];
        $(this).find('option:selected').each(function(){ items.push($(this).val()); });
        checkSelectorValue("#select_Specialite", items_specialite);

        if(items.length>0) {
            $.ajax({
                url: "/ville/getvillesbybrickims/",
                data: {brickims_id: items, specialite_id: items_specialite, titre_id: [9]},
                dataType: 'json',
                type: 'POST'
            }).done(function (data, status) {
                if (status === "success") {
                    var text = "<option value='0'>Tous</option>";
                    $.each(data, function (i, row) {
                        text += "<option value='" + row.id + "'>" + row.name + "</option>";
                    });
                    $("#select_Ville").html(text);
                    getProspectCount();
                } else {
                    alert("Une erreur s'est produite");
                }
            });
        }
        else {
            alert("BrickIMS vide");
        }
    });
    // get available ville by brickims
    $("#select_Ville").change(function(){
        getProspectCount();
    });

    $("#show_selectprospects").click(function(){getProspect()});
    getProspectCount();
    getProspect();
    $("#select_Specialite").trigger('click');
});

function getProspectCount(){

    var items_ville = [];
    var items_specialite = [];
    var items_brickims = [];

    var actionFrom = $(this).attr('id');

    checkSelectorValue("#select_Specialite", items_specialite);
    checkSelectorValue("#select_Ville", items_ville);
    checkSelectorValue("#select_BrickIMS", items_brickims);

    $.ajax({
        url: "/prospect/countprospectfilterprivate/",
        data: {
            specialite : items_specialite,
            ville : items_ville,
            brickims : items_brickims,
            titre : [9],
            privateOnly : true
        },
        dataType: 'json',
        type: 'POST'
    }).done(function(data, status) {
        if ( status === "success") {
            $("#total_filtre").html(data.private);
            $("#total_filtre_notprivate").html(data.notprivate);
        } else {
            alert("Une erreur s'est produite");
        }
    });
};

function getProspect()
{

    oTable = $('.datatable-prospect').dataTable();
    oTable.fnClearTable(false);
    oTable.fnDestroy();
    $('.datatable-prospect').dataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 100,
        "ajax": {
            "url": "/prospect/paging",
            "type": "POST",
            "data": function ( d ) {
                var items_specialite = [];
                var items_ville = [];
                var items_brickims = [];
                checkSelectorValue("#select_Specialite", items_specialite);
                checkSelectorValue("#select_Ville", items_ville);
                checkSelectorValue("#select_BrickIMS", items_brickims);

                d.specialite = items_specialite;
                d.ville = items_ville;
                d.brickims = items_brickims;
                d.privateOnly = true;
                d.titre = [9];
                return d;
            }
        },
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/French.json"
        }
    });

}
function checkSelectorValue(selector, items)
{
    if($(selector).find('option:selected').val() == 0 || typeof $(selector).find('option:selected').val() == 'undefined')
    {
        $(selector).find('option:not(selected)').each(function(){if($(this).val() != 0 ) items.push($(this).val()); });
    }
    else {
        $(selector).find('option:selected').each(function(){items.push($(this).val()); });
    }
}