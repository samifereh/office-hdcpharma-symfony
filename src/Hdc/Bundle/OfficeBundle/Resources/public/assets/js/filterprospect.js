$(document).ready(function() {

    $(".select_filtres").change(function(){
        var nextSelectors = $(this).parent().nextAll();
        $(this).
            find('option:selected').
            each(function(){
                                if ($(this).val()==0) {
                                    nextSelectors.find('option').removeAttr("selected");
                                    nextSelectors.find('.select_filtres').val(0);
                                }
                            });

    });

    // get gouvernorats by region
    $("#select_Region").change(function(){
        var items = [];
        $(this).find('option:selected').each(function(){ items.push($(this).val()); });

        $.ajax({
            url: "/gouvernorat/getgouvernorat_by_region/",
            data: {region_id : items},
            dataType: 'json',
            type: 'POST'
        }).done(function(data, status) {
            if ( status === "success") {
                var text = "<option value='0'>Tous</option>";
                $.each(data, function (i, row) {
                    text += "<option value='"+row.id+"'>"+row.name+"</option>";
                });
                $("#select_Gouvernorat").html(text);
            } else {
                alert("Une erreur s'est produite");
            }
        });
    });

    // get brickims by gouvernorats
    $("#select_Gouvernorat").change(function(){
        var items = [];
        $(this).find('option:selected').each(function(){ items.push($(this).val()); });

        $.ajax({
            url: "/brickims/getbrick_by_gouvernorat/",
            data: {gouv_id : items},
            dataType: 'json',
            type: 'POST'
        }).done(function(data, status) {
            if ( status === "success") {
                var text = "<option value='0'>Tous</option>";
                $.each(data, function (i, row) {
                    text += "<option value='"+row.id+"'>"+row.name+"</option>";
                });
                $("#select_BrickIMS").html(text);
            } else {
                alert("Une erreur s'est produite");
            }
        });
    });

    // get available ville by brickims
    $("#select_BrickIMS").change(function(){
        var items = [];
        $(this).find('option:selected').each(function(){ items.push($(this).val()); });

        $.ajax({
            url: "/ville/getallvillesbybrickims/",
            data: {brickims_id : items},
            dataType: 'json',
            type: 'POST'
        }).done(function(data, status) {
            if ( status === "success") {
                var text = "<option value='0'>Tous</option>";
                $.each(data, function (i, row) {
                    text += "<option value='"+row.id+"'>"+row.name+"</option>";
                });
                $("#select_Ville").html(text);
            } else {
                alert("Une erreur s'est produite");
            }
        });
    });

    // get available specialite in prospect by ville
    $("#select_Ville").change(function(){
        var items = [];
        $(this).find('option:selected').each(function(){ items.push($(this).val()); });

        $.ajax({
            url: "/prospect/getavailable_specialite_by_ville/",
            data: {ville_id : items},
            dataType: 'json',
            type: 'POST'
        }).done(function(data, status) {
                if ( status === "success") {
                    var text = "<option value='0'>Tous</option>";
                    $.each(data, function (i, row) {
                        text += "<option value='"+row.id+"'>"+row.name+"</option>";
                    });
                    $("#select_Specialite").html(text);
                } else {
                    alert("Une erreur s'est produite");
                }
            });
    });


    $(".select_filtres").click(function(){

        var items_ville = [];
        var items_spec = [];
        var items_titre = [];
        var items_potentiel = [];
        var items_cursus = [];
        var items_brickims = [];
        $("#select_Specialite").find('option:selected').each(function(){items_spec.push($(this).val()); });
        $("#select_Ville").find('option:selected').each(function(){items_ville.push($(this).val()); });
        $("#select_Titre").find('option:selected').each(function(){items_titre.push($(this).val()); });
        $("#select_Potentiel").find('option:selected').each(function(){items_potentiel.push($(this).val()); });
        $("#select_Cursus").find('option:selected').each(function(){items_cursus.push($(this).val()); });
        $("#select_BrickIMS").find('option:selected').each(function(){items_brickims.push($(this).val()); });

        $.ajax({
            url: "/prospect/countprospectfilter/",
            data: {
                specialite : items_spec,
                ville : items_ville,
                titre : items_titre,
                potentiel : items_potentiel,
                cursus : items_cursus,
                brickims : items_brickims,
            },
            dataType: 'json',
            type: 'POST'
        }).done(function(data, status) {
            if ( status === "success") {
                $("#total_filtre").html(data);
            } else {
                alert("Une erreur s'est produite");
            }
        });
    });
    $("#show_selectprospects").click(function(){

        oTable = $('.datatable-prospect').dataTable();
        oTable.fnClearTable();
        oTable.fnDestroy();
        $('.datatable-prospect').dataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": 100,
            "ajax": {
                "url": "/prospect/paging",
                "type": "POST",
                "data": function ( d ) {
                    var items_ville = [];
                    var items_spec = [];
                    var items_titre = [];
                    var items_potentiel = [];
                    var items_cursus = [];
                    var items_brickims = [];
                    $("#select_Specialite").find('option:selected').each(function(){items_spec.push($(this).val()); });
                    $("#select_Ville").find('option:selected').each(function(){items_ville.push($(this).val()); });
                    $("#select_Titre").find('option:selected').each(function(){items_titre.push($(this).val()); });
                    $("#select_Potentiel").find('option:selected').each(function(){items_potentiel.push($(this).val()); });
                    $("#select_Cursus").find('option:selected').each(function(){items_cursus.push($(this).val()); });
                    $("#select_BrickIMS").find('option:selected').each(function(){items_brickims.push($(this).val()); });
                    d.specialite = items_spec;
                    d.ville = items_ville;
                    d.titre = items_titre;
                    d.potentiel = items_potentiel;
                    d.cursus = items_cursus;
                    d.brickims = items_brickims;
                }
            },
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/French.json"
            }
        });

    });


    $('#myModal').on('shown.bs.modal', function(e) {
        var id = $(e.relatedTarget).attr('data-id');
        $( ".modal-content" ).load( "/visite/add/"+id, function() {

        });
    })



});