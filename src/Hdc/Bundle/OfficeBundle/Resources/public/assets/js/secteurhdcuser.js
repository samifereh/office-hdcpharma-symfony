$(document).ready(function() {

    $('#btn-add-secteurhdcuser').click(function(){
        $('#select-from option:selected').each( function() {
            $('#select-to').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
        return false;
    });
    $('#btn-remove-secteurhdcuser').click(function(){
        $('#select-to option:selected').each( function() {
            $('#select-from').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
        return false;
    });

    $("form[name='secteurhdcuser']").submit(function(){
        if ( $('#select-to option').length > 0 )
            $('#select-to option').attr('selected', true);
        else {
            alert("Veuillez séléctionner un ou plusieurs secteurs HDC")
            return false;
        }
    });

});