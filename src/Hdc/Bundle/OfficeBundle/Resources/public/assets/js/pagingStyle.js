$(document).ready(function () {
    if ($('.datatable-prospect').length>0) {
        $('.datatable-prospect').dataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": 100,
            "ajax": {
                "url": "/prospect/paging",
                "type": "POST"
            },
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/French.json"
            }
        });
    }
});