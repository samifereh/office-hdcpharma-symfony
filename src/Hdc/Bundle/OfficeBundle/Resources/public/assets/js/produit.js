$(document).ready(function() {

    $("#prix_ventettc").blur(function(){
        if ( $.isNumeric($(this).val()) && $(this).val()) {
            var taux = $("#brickims_tauxtva_id option:selected").text();
            var ttc= $(this).val();

            var ht = (parseFloat(ttc)/((taux/100)+1));

            $("#brickims_prix_venteht").val( ht.toFixed(3) );
        }
    });

});