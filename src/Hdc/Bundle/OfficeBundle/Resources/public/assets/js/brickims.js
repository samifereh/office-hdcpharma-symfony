$(document).ready(function() {

    $('#btn-add-ville').click(function(){
        $('#select-from option:selected').each( function() {
            $('#select-to').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
        return false;
    });
    $('#btn-remove-ville').click(function(){
        $('#select-to option:selected').each( function() {
            $('#select-from').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
        return false;
    });

    $("form[name='brickims']").submit(function(){
        if ( $('#select-to option').length > 0 )
            $('#select-to option').attr('selected', true);
        else {
            alert("Veuillez séléctionner une ou plusieurs localités")
            return false;
        }
    });


    $("#select_regions").change(function(){
        var selectitem = [];
        selectitem.push($(this).find('option:selected').val());

        // get gouvernorats by region
        $.ajax({
            url: "/gouvernorat/getgouvernorat_by_region/",
            data: {region_id : selectitem},
            dataType: 'json',
            type: 'POST'
        }).done(function(data, status) {
                if ( status === "success") {
                    var text = "<option value=''>Sélectionner un gouvernorat</option>";
                    $.each(data, function (i, row) {
                        text += "<option value='"+row.id+"'>"+row.name+"</option>";
                    });
                    $("#brickims_gouvernorat_id").html(text);
                } else {
                    alert("Une erreur s'est produite");
                }
            });
    });

    $("#brickims_gouvernorat_id").change(function(){
        var selectitem = [];
        selectitem.push($(this).find('option:selected').val());

        // get ville by gouvernorats
        $.ajax({
            url: "/ville/getvilles_by_gouvernorat/",
            data: {gouv_id : selectitem},
            dataType: 'json',
            type: 'POST'
        }).done(function(data, status) {
                if ( status === "success") {
                    var text = "";
                    $.each(data, function (i, row) {
                        text += "<option value='"+row.id+"'>"+row.name+"</option>";
                    });
                    $("#select-from").html(text);
                } else {
                    alert("Une erreur s'est produite");
                }
            });
    });

});