//cacher le form de password
$(document).ready(function(){
    // On cache le div a afficher :
    $( ".cbxShowHide" ).each(function() {
        var target = $(this).data("target");
        var visibility = $(this).data("target-visibility");
        if(!visibility)
            $(target).hide();
    });
    // Tester selon le checkbox pour caher/afficher le form password
    $('.cbxShowHide').change(function(){
        var target = $(this).data("target");
        var visibility = $(this).data("target-visibility");
        if(this.checked){
            $(target).show();
        }
        else
            $(target).fadeOut('slow');
    });

    //Fonction permettant le choix unique d'un optgroup appartenant à un optiongroupe

    var memoryOne;
    var memoryTwo;
    $(".select-secteurs option").mouseover(function () {
        var selectedOne = $(".select-secteurs optgroup:nth-of-type(1)").children("option:selected").index();
        var selectedTwo = $(".select-secteurs optgroup:nth-of-type(2)").children("option:selected").index();
        memoryOne = selectedOne;
        memoryTwo = selectedTwo;
    }).click(function () {
        var theSelectedIndex = $(this).index();
        var theParentIndex = $(this).parent().index();
        setTimeout(function () {
            clickEvent(theSelectedIndex, theParentIndex, memoryOne, memoryTwo);
        }, 1);
    });

    function clickEvent(passedIndex, parentIndex, memoryOne, memoryTwo) {
        var selectionOne = memoryOne;
        var selectionTwo = memoryTwo;
        var theParent = $(".select-secteurs  optgroup:eq(" + parentIndex + ")");
        var theOption = $(".select-secteurs  optgroup:eq(" + parentIndex + ") option:eq(" + passedIndex + ")");
        var theGrandParent = $("select");
        theParent.find("option:not(option:eq(" + passedIndex + "))").prop("selected", false);

        if (parentIndex == 0) {
            $(".select-secteurs optgroup:not(optgroup:eq(" + parentIndex + "))").children("option:eq(" + selectionTwo + ")").prop("selected", true);
        } else if (parentIndex == 1) {
            $(".select-secteurs optgroup:not(optgroup:eq(" + parentIndex + "))").children("option:eq(" + selectionOne + ")").prop("selected", true);
        }
    }

});
