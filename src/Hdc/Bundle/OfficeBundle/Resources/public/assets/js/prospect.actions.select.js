$(document).ready(function() {
    (function ($) {

        function doActions(selector, manipArray){
            if (manipArray.text && $(selector+' option:selected').text() == manipArray.text) {
                if (manipArray.events) {
                    $.each(manipArray.events, function (element, actions) {
                        $.each(actions, function (key, action) {
                            switch (key) {
                                case 'val':
                                    $(element).val(action);
                                    break;
                                case 'css':
                                    $(element).css(action);
                                    break;
                                case 'prop':
                                {
                                    $(element + " option[value*='" + action.target + "']").prop(
                                        action.name, action.value
                                    );
                                }
                                    break;
                            }
                        });

                    });
                }
            }
        }
        $.each(manip, function (selector, sectionManip) {
            $(selector).change(function () {
                    doActions(selector, sectionManip);
                }
            );
        });
    })(jQuery);
});