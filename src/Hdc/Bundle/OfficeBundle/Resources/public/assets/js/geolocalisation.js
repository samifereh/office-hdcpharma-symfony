function calculateDistance(lat1, lon1, lat2, lon2) {
    var R = 6371; // km
    var dLat = (lat2 - lat1).toRad();
    var dLon = (lon2 - lon1).toRad();
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
}
Number.prototype.toRad = function() {
    return this * Math.PI / 180;
}
// check for Geolocation support
if (navigator.geolocation) {
    console.log('Geolocation is supported!');

    $(document).ready(function(){

        navigator.geolocation.watchPosition(function(position) {
            $('#setCurrentLocation').click(function () {
                $("#prospect_lat").val(position.coords.latitude);
                $("#prospect_lng").val(position.coords.longitude);
            });
            document.getElementById('currentLat').innerHTML = position.coords.latitude;
            document.getElementById('currentLon').innerHTML = position.coords.longitude;
            //document.getElementById('distance').innerHTML = calculateDistance(position.coords.latitude, position.coords.longitude, 36.831553,10.207990 );
        });


    });


}
else {
    console.log('Geolocation is not supported for this Browser/OS version yet.');
}