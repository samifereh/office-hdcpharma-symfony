$(document).ready(function() {

    $(document).on('change','.produitselect',function(){
        var selectitem = $(this).find('option:selected').val();
        var selected = $(this);
        // get substitude products
        $.ajax({
            url: "/commande/getsubstituteproducts/"+selectitem,
            dataType: 'json'
        }).done(function(data, status) {
                if ( status === "success") {
                    var text = "";
                    $.each(data, function (i, row) {
                        if (row.id == selectitem)
                            sel = "SELECTED";
                        else
                            sel = "";
                        text += "<option value='"+row.id+"' "+sel+">"+row.name+"</option>";

                        selected.parent().parent().find(".produitgratuit").html(text);

                        /* set authorized qte */
                        selected.parent().parent().find("input[name=quantitegratuiteauthorisee]").val(row.qtefree);
                        selected.parent().parent().find(".smaxvlue").attr("max",row.qtefree);

                    });
                } else {
                    alert("Une erreur s'est produite");
                }
            });
    });

    $(document).on('blur','.smaxvlue', function(){
        //check valid row

        var val = parseInt($(this).val());
        var max = parseInt($(this).attr("max"));
        if (val > max) {
            alert("Qunatité maximal autorisée ="+ $(this).attr("max"));
            $(this).val($(this).attr("max"));
            $(this).focus();
            return false;
        }

    });

});