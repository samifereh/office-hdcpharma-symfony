$(document).ready(function(){
    $("form[name='bongratuite']").submit(function(){

        var isOk=true;

        $( "input[name^='quantite']").each(function(){
            var elem = $(this).val();
            if ( !($.isNumeric(elem)) ) {
                $(this).focus();
                isOk = false;
            }
        });

        if (!isOk) return false;
    });

});
