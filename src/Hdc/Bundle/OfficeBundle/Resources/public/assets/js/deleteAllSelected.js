$(document).ready(function() {

    //séléctionner les checkbox à supprimer
    $('#master').on('click', function(e) {
        if($(this).is(':checked',true))
        {
            $(".sub_chk").prop('checked', true);
        }
        else
        {
            $(".sub_chk").prop('checked',false);
        }
    });

    //action de suppression via checkbox
    $('.delete_all').on('click', function(e) {
    var allVals = [];
    var URL = $(location).attr('href');
    $(".sub_chk:checked").each(function() {
        allVals.push($(this).attr('data-id'));
    });
    if(allVals.length <=0)
    {
        alert("Veuillez séléctionner une ligne à supprimer");
    }
    else {
        WRN_PROFILE_DELETE = "Etes vous sûr?";
        var check = confirm(WRN_PROFILE_DELETE);
        var URL = $("#URL").text().replace(/\//g,'');
        if(check == true){
            //for server side
            var join_selected_values = allVals.join(",");
            console.log('Vous avez séléctionné : '+allVals.length);
            $.ajax({
                type: "POST",
                url: "/visite/deleteSelected/",
                cache:false,
                data: {'ids':allVals, 'url':URL},
                success: function(response)
                {
                    location.reload();
                }
            });
        }
        else{
            $("#master").prop('checked',false);
            $(".sub_chk").prop('checked',false);
        }
    }
});

});