$(document).ready(function(){

    var jsonContentComplex =
        [/*
            {
                "number": 1,
                "first_name": "John",
                "last_name": "Appleseed",
                "email": "john@appleseed.com",
                "sex": "Male",
                "techniques":
                    [
                        "Can vanish",
                        "Is strong",
                        "Like Apples"
                    ],
                "location": "United States"
            },
            {
                "number": 1,
                "first_name": "João",
                "last_name": "Canabrava",
                "email": "joao@canabrava.com",
                "sex": "Male",
                "techniques":
                    [
                        "Is always drunk",
                        "Stronger liver",
                        "Caipirinha adept"
                    ],
                "location": "Brazil"
            },
            {
                "number": 3,
                "first_name": "Patrick",
                "last_name": "Grapeseed",
                "email": "patrick@grapeseed.com",
                "sex": "Male",
                "techniques":
                    [
                        "Hates grapes",
                        "It has a grape head"
                    ],
                "location": "United Kingdom"
            },
            {
                "number": 4,
                "first_name": "Xingling",
                "last_name": "Ping Pong",
                "email": "xingling@pingpong.com",
                "sex": "Female",
                "techniques":
                    [
                        "Works at AliExpress",
                        "Slavery adept"
                    ],
                "location": "China"
            }*/
        ];

    $('#bon-gratuite-details').mounTable(jsonContentComplex,
        {

            /* The model class */

            model: '.mountable-model',

            /* No console messages */

            noDebug: false,

            /* Options to your new line button */

            addLine:
            {

                /* New line button selector */

                button: "#button-addnew-record",

                /* Callback function */

                onClick: function (element)
                {
                    // TODO test if last row is valid
                    console.log('Line added!');
                    return true;
                }
            },

            /* Options to your delete line button */

            deleteLine:
            {

                /* Delete line button selector */

                button: ".mountable-remove-line-2",

                /* Callback function */

                onClick: function (element)
                {
                    if (confirm("Are you sure?"))
                    {
                        console.log('Line removed!');
                        return true;
                    }
                }

            }

        });

});