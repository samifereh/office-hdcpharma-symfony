$(document).ready(function() {

    $(".change_statusform").submit(function(){
        if (!confirm('Etes vous sûr?')) return false;

        var form = $(this);
        var commandeid = $(this).find('input[name=idcommande]').val();
        var status_selected = $(this).find('select[name=statuschange]').find(":selected").val();


        $.ajax({
            url: "/commande/setstatus/"+commandeid+"/"+status_selected,
            dataType: 'json'
        }).done(function(data, status) {
                if ( status === "success") {
                    if (status_selected==2) form.html("Commande livrée");
                } else {
                    alert("Une erreur s'est produite");
                }
            });

        return false;
    });

});