$(document).ready(function() {
    $("#slideDownTable").click(function() {
        $("#RegionTable").toggle(800);
        $("#HideShowDiv").find('i').toggleClass('fa fa-chevron-down').toggleClass('fa fa-chevron-right');
        return false;
    });

    $(function () {
        $(".navbarRegion").hide();
        $(window).scroll(function () {
            // set distance user needs to scroll before we fadeIn navbar
            if ($(this).scrollTop() > 800) {
                $('.navbarRegion').fadeIn();
                $("#RegionTable").insertAfter("#navBarTitle");
                $("#newProspectButton").insertAfter("#SearchProspectButton");
            } else {
                $('.navbarRegion').fadeOut();
                $("#newProspectButton").insertAfter("#PreAddButton");
                $("#RegionTable").insertAfter("#slideDownTable");
            }
        });
    });
})