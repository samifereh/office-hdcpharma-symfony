$(document).ready(function() {

    $('#btn-add-produit').click(function(){
        $('#select-from option:selected').each( function() {
            $('#select-to').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
        return false;
    });
    $('#btn-remove-produit').click(function(){
        $('#select-to option:selected').each( function() {
            $('#select-from').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
        return false;
    });


    $('#btn-add-specialite').click(function(){
        $('#select-from2 option:selected').each( function() {
            $('#select-to2').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
        return false;
    });
    $('#btn-remove-specialite').click(function(){
        $('#select-to2 option:selected').each( function() {
            $('#select-from2').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
        return false;
    });


    $("form[name='gamme']").submit(function(){
        if ( $('#select-to option').length > 0 && $('#select-to2 option').length > 0 ) {
            $('#select-to').val(getOptions("#select-to"));
            $('#select-to2').val(getOptions("#select-to2"));
        } else {
            if ($('#select-to option').length == 0) alert("Veuillez séléctionner un ou plusieurs produits")
            if ($('#select-to2 option').length == 0) alert("Veuillez séléctionner une ou plusieurs spécialités")
            return false;
        }
    });

    function getOptions(selector) {
        var values = [];
        $(selector+' option').each(function(index, option){
            values.push($(option).val());
        });
        return values;
    }
});