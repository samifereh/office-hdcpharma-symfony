/**
 * Created by sami on 17/08/2017.
 */
var manip = {
    "#prospect_titre_id": {
        "text": "Privé",
        "events": {
            "#prospect_genre_id": {'val': 2},
            "#prospect_potentiel_id": {
                'val': 5,
                'prop': {
                    target: "6",
                    name : "disabled",
                    value: true
                }
            },
            "#prospect_specialite_id": {
                'val': '',
                'prop': {
                    target: "16",
                    name : "disabled",
                    value: false
                }
            },
        /*
            "#prospect_lastname": {
                'css':{
                    'background-color':'yellow',
                    'border': "solid 2px"
                }
            }
        */
        }
    },
    "#prospect_specialite_id": {
        "text": "SF",
        "events": {
            "#prospect_titre_id": {'val': 7},
            "#prospect_genre_id": {'val': 1},
            "#prospect_potentiel_id": {
                'val': 6
            }
        }
    },
};