<?php

namespace Hdc\Bundle\OfficeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Hdc\Bundle\OfficeBundle\Entity\BrickIMS;
use Hdc\Bundle\OfficeBundle\Entity\Ville;

class CheckImportSecteurIMSCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('secteurims:check')
            ->setDescription('Import secteurims')
            ->addArgument(
                'filename',
                InputArgument::OPTIONAL,
                'the file must be in CSV format?'
            )
            ->addOption(
                'force',
                null,
                InputOption::VALUE_NONE,
                'If set, generate a csv file with approved lines and a csv file with failed lines'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('filename');
        if (file_exists($filename)) {
            $row = 0;

            $repo = $this->getContainer()->get('doctrine')
                ->getRepository('HdcOfficeBundle:Ville');
            $available_cities = $repo->getAll();
            foreach ($available_cities as $city)
                $villes[$city[0]->getId()] = $city[0]->getName();

            $repo = $this->getContainer()->get('doctrine')
                ->getRepository('HdcOfficeBundle:BrickIMS');
            $bricksims_list = $repo->findAll();

            foreach ($bricksims_list as $brickims)
                $brickimstab[$brickims->getId()] = $brickims->getName();

            $tableau=array();
            $tabValid=array();
            $tabFailed=array();
            if (($handle = fopen($filename, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 4096, ";")) !== FALSE) {
                    $validLine = true;

                    if ($data && array_search(trim($data[0]),$villes))
                        $tableau[$row]["ville"]= array_search(trim($data[0]),$villes);
                    else {
                        $tableau[$row]["ville"]=trim($data[0]);
                        $validLine=false;
                    }

                    if ($data && array_search(trim($data[1]),$brickimstab))
                        $tableau[$row]["brickims"]= array_search(trim($data[1]),$brickimstab);
                    else {
                        $tableau[$row]["brickims"]=trim($data[1]);
                        $validLine=false;
                    }

                    if ($validLine) {
                        $exist = false;
                        if (count($tabValid)>0) {
                            foreach($tabValid as $line){

                                if ($tableau[$row]["brickims"] == $line["brickims"] && $tableau[$row]["ville"] == $line["ville"] )
                                {
                                    $exist = true;
                                    break;
                                }


                            }
                        }
                        if (!$exist)
                            $tabValid[]=$tableau[$row];

                    } else
                        $tabFailed[]=$tableau[$row];

                    $row++;
                }
                fclose($handle);
            }

            $output->writeln("Nb valid lines :".count($tabValid));
            $output->writeln("Nb failed lines :".count($tabFailed));
            $output->writeln("Total :".$row);
        }

        if ($input->getOption('force')) {
            // save files
            $fp = fopen('valid.csv', 'w');

            foreach ($tabValid as $fields) {
                fputcsv($fp, $fields, ";");
            }

            fclose($fp);

            $fp = fopen('failed.csv', 'w');

            foreach ($tabFailed as $fields) {
                fputcsv($fp, $fields, ";");
            }

            fclose($fp);
        }

        //$output->writeln($text);
    }
}