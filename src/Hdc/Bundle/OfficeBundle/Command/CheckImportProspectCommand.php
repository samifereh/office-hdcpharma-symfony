<?php

namespace Hdc\Bundle\OfficeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Hdc\Bundle\OfficeBundle\Entity\BrickIMS;
use Hdc\Bundle\OfficeBundle\Entity\Ville;

class CheckImportProspectCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('prospect:check')
            ->setDescription('Import Prospect')
            ->addArgument(
                'filename',
                InputArgument::OPTIONAL,
                'the file must be in CSV format?'
            )
            ->addOption(
                'force',
                null,
                InputOption::VALUE_NONE,
                'If set, generate a csv file with approved lines and a csv file with failed lines'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('filename');
        if (file_exists($filename)) {
            $row = 0;

            $repo = $this->getContainer()->get('doctrine')
                ->getRepository('HdcOfficeBundle:Ville');
            $available_cities = $repo->getAll();
            foreach ($available_cities as $city)
                $villes[$city[0]->getId()] = $city[0]->getName();

            $repo = $this->getContainer()->get('doctrine')
                ->getRepository('HdcOfficeBundle:BrickIMS');
            $bricksims_list = $repo->findAll();

            foreach ($bricksims_list as $brickims)
                $brickimstab[$brickims->getId()] = $brickims->getName();

            $repo = $this->getContainer()->get('doctrine')
                ->getRepository('HdcOfficeBundle:Potentiel');
            $potentiels = $repo->findAll();

            $potentieltab=array();
            foreach ($potentiels as $pot)
                $potentieltab[$pot->getId()] = $pot->getName();

            $repo = $this->getContainer()->get('doctrine')
                ->getRepository('HdcOfficeBundle:Specialite');
            $Specialites = $repo->findAll();

            $SpecialiteTab =array();
            foreach ($Specialites as $sp)
                $SpecialiteTab[$sp->getId()] = $sp->getName();

            $repo = $this->getContainer()->get('doctrine')
                ->getRepository('HdcOfficeBundle:Titre');
            $titres = $repo->findAll();

            $titreTab =array();
            foreach ($titres as $t)
                $titreTab[$t->getId()] = $t->getName();

            $repo = $this->getContainer()->get('doctrine')
                ->getRepository('HdcOfficeBundle:Cursus');
            $cursus = $repo->findAll();

            $cursusTab =array();
            foreach ($cursus as $c)
                $cursusTab[$c->getId()] = $c->getName();

            $repo = $this->getContainer()->get('doctrine')
                ->getRepository('HdcOfficeBundle:Genre');
            $genre = $repo->findAll();

            $genreTab =array();
            foreach ($genre as $g)
                $genreTab[$g->getId()] = $g->getName();

            $tableau=array();
            $tabValid=array();
            $tabFailed=array();
            if (($handle = fopen($filename, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 4096, ";")) !== FALSE) {
                    $validLine = true;

                    if ($data && array_search($data[0],$villes))
                        $tableau[$row]["ville_id"]= array_search($data[0],$villes);
                    else {
                        $tableau[$row]["ville_id"]=$data[0];
                        $validLine=false;
                    }

                    if ($data && array_search($data[1],$brickimstab))
                        $tableau[$row]["brickims_id"]= array_search($data[1],$brickimstab);
                    else {
                        $tableau[$row]["brickims_id"]=$data[1];
                        $validLine=false;
                    }

                    if ($data[2]) $tableau[$row]["adresse"]= $data[2]; else $tableau[$row]["adresse"]= "";

                    if (trim($data[3]))
                        $tableau[$row]["lastname"]= $data[3];
                    else
                        $tableau[$row]["lastname"]= "";

                    if (trim($data[4]))
                        $tableau[$row]["firstname"]= $data[4];
                    else
                        $tableau[$row]["firstname"]= "";


                    if (!$tableau[$row]["lastname"] && !$tableau[$row]["firstname"]) {
                        $tableau[$row]["lastname"]="1";
                    } elseif (!$tableau[$row]["lastname"] && $tableau[$row]["firstname"]) {
                        $tableau[$row]["lastname"]=$tableau[$row]["firstname"];
                    }


                    if ($data && array_search($data[5],$potentieltab))
                        $tableau[$row]["potentiel_id"]= array_search($data[5],$potentieltab);
                    else {
                        if ($data[7] == "Privé")
                            $tableau[$row]["potentiel_id"]=3;
                        else
                            $tableau[$row]["potentiel_id"]=6;
                    }

                    if ($data && array_search($data[6],$SpecialiteTab))
                        $tableau[$row]["specialite_id"]= array_search($data[6],$SpecialiteTab);
                    else {
                        $tableau[$row]["specialite_id"]=$data[6];
                        $validLine=false;
                    }

                    if ($data && array_search($data[7],$titreTab))
                        $tableau[$row]["titre_id"]= array_search($data[7],$titreTab);
                    else {
                        $tableau[$row]["titre_id"]=$data[7];
                        $validLine=false;
                    }

                    if ($data[8]) $tableau[$row]["remarque"]= $data[8]; else $tableau[$row]["remarque"]= "";

                    if ($data && array_search($data[9],$cursusTab))
                        $tableau[$row]["cursus_id"]= array_search($data[9],$cursusTab);
                    else {
                        $tableau[$row]["cursus_id"]=$data[9];
                        $validLine=false;
                    }

                    if ($data && array_search($data[10],$genreTab))
                        $tableau[$row]["genre_id"]= array_search($data[10],$genreTab);
                    else {
                        $tableau[$row]["genre_id"]=$data[10];
                        $validLine=false;
                    }


                    if ($validLine)
                        $tabValid[]=$tableau[$row];
                    else
                        $tabFailed[]=$tableau[$row];

                    $row++;
                }
                fclose($handle);
            }

            $output->writeln("Nb valid lines :".count($tabValid));
            $output->writeln("Nb failed lines :".count($tabFailed));
            $output->writeln("Total :".$row);
        }

        if ($input->getOption('force')) {
            // save files
            $fp = fopen('valid.csv', 'w');

            foreach ($tabValid as $fields) {
                fputcsv($fp, $fields, ";");
            }

            fclose($fp);

            $fp = fopen('failed.csv', 'w');

            foreach ($tabFailed as $fields) {
                fputcsv($fp, $fields, ";");
            }

            fclose($fp);
        }

        //$output->writeln($text);
    }
}