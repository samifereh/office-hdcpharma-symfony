<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CommandeDetail extends EntityRepository
{
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT d, p.id as pid, p.name, pg.id as pgid, pg.name as pgname
                               FROM HdcOfficeBundle:CommandeDetail d
                               INNER JOIN HdcOfficeBundle:Produit p
                                  WITH d.produit = p.id
                               INNER JOIN HdcOfficeBundle:Produit pg
                                  WITH d.produitgratuit = pg.id
                                  WHERE d.commande = ".$criteria["commande"]
            );
        return $q->getResult();
    }

}

