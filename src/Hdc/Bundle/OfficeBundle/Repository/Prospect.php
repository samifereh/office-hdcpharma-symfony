<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityRepository;

class Prospect extends EntityRepository
{

    public function findAll($userManager = null)
    {
        $list = $this->getSecteurHDCByCurrentUser($userManager);
        if ( $list )
            $list = implode(",", $list);

        $q = $this->getEntityManager()
            ->createQuery("SELECT prospect,
                                  specialite.id,
                                  specialite.name as spname,
                                  ville.id, ville.name as villename,
                                  titre.id, titre.name as trname,
                                  curs.id, curs.name as cursusname,
                                  genre.id, genre.name as genrename,
                                  gov.id, gov.name as govname,
                                  secteur.id, secteur.name as sectname,
                                  brick.id, brick.name as brickname,
                                  pot.id as potid, pot.name as potname
                               FROM HdcOfficeBundle:Prospect prospect
                               INNER JOIN HdcOfficeBundle:Specialite specialite
                                  WITH prospect.specialite_id = specialite.id
                               LEFT JOIN HdcOfficeBundle:Titre titre
                                  WITH prospect.titre_id = titre.id
                               LEFT JOIN HdcOfficeBundle:Cursus curs
                                  WITH prospect.cursus_id = curs.id
                               LEFT JOIN HdcOfficeBundle:Genre genre
                                  WITH prospect.genre_id = genre.id
                               INNER JOIN HdcOfficeBundle:Ville ville
                                  WITH prospect.ville_id = ville.id
                               INNER JOIN HdcOfficeBundle:Gouvernorat gov
                                  WITH ville.gouvernorat_id = gov.id
                               INNER JOIN HdcOfficeBundle:BrickIMS brick
                                  WITH prospect.brickims_id = brick.id
                               LEFT JOIN HdcOfficeBundle:SecteurHDC secteur
                                  WITH prospect.secteurhdc_id = secteur.id
                               LEFT JOIN HdcOfficeBundle:Potentiel pot
                                  WITH prospect.potentiel_id = pot.id
                               ".(($list)?"WHERE prospect.secteurhdc_id IN (".$list.")":""));
        return $q->getResult();
    }

    public function findAllProspects($userManager = null)
    {
        $list = $this->getSecteurHDCByCurrentUser($userManager);
        if ( $list )
            $list = implode(",", $list);

        $q = $this->getEntityManager()
            ->createQuery("SELECT prospect, specialite.id, specialite.name as spname,
                                  ville.id, ville.name as villename,
                                  titre.id, titre.name as trname,
                                  curs.id, curs.name as cursusname,
                                  genre.id, genre.name as genrename,
                                  gov.id, gov.name as govname,
                                  secteur.id, secteur.name as sectname,
                                  brick.id, brick.name as brickname,
                                  pot.id as potid, pot.name as potname
                               FROM HdcOfficeBundle:Prospect prospect
                               INNER JOIN HdcOfficeBundle:Specialite specialite
                                  WITH prospect.specialite_id = specialite.id
                               LEFT JOIN HdcOfficeBundle:Titre titre
                                  WITH prospect.titre_id = titre.id
                               LEFT JOIN HdcOfficeBundle:Cursus curs
                                  WITH prospect.cursus_id = curs.id
                               LEFT JOIN HdcOfficeBundle:Genre genre
                                  WITH prospect.genre_id = genre.id
                               INNER JOIN HdcOfficeBundle:Ville ville
                                  WITH prospect.ville_id = ville.id
                               INNER JOIN HdcOfficeBundle:Gouvernorat gov
                                  WITH ville.gouvernorat_id = gov.id
                               INNER JOIN HdcOfficeBundle:BrickIMS brick
                                  WITH prospect.brickims_id = brick.id
                               LEFT JOIN HdcOfficeBundle:SecteurHDC secteur
                                  WITH prospect.secteurhdc_id = secteur.id
                               LEFT JOIN HdcOfficeBundle:Potentiel pot
                                  WITH prospect.potentiel_id = pot.id
                               ".(($list)?"WHERE prospect.secteurhdc_id IN (".$list.")":""))
            ->setFirstResult(0)
            ->setMaxResults(100);
        return $q->getResult();
    }

    public function createQueryBuilderHDC($alias, $userManager, $isFournisseur=false)
    {

        $list = $this->getSecteurHDCByCurrentUser($userManager);
        if ( $list )
            $list = implode(",", $list);

        $q = $this->_em->createQueryBuilder()
            ->select($alias)
            ->from($this->_entityName, $alias);

        if ( $isFournisseur )  {

            $q= $q->innerJoin("HdcOfficeBundle:Specialite","sp","WITH", "sp.id = ".$alias.".specialite_id")
                ->where("sp.isfournisseur=1");

            if ( $list )
                $q = $q->where("sp.isfournisseur=1 AND ". $alias.".secteurhdc_id IN (".$list.")");
        } else {
            if ( $list )
                $q = $q->where($alias.".secteurhdc_id IN (".$list.")");
        }

        $q = $q->orderBy($alias.".lastname", "ASC");
        return $q;

    }

    public function getSecteurHDCByCurrentUser($userManager) {

        $user = $userManager->getCurrentUser();
        $list= "";
        if ( $user->hasRole("ROLE_COMMERCIAL") ) {

            $q = $this->getEntityManager()
                ->createQuery("SELECT sectuser, secteurhdc.id as idsecteur
                               FROM HdcOfficeBundle:SecteurHDCUser sectuser
                               INNER JOIN HdcOfficeBundle:SecteurHDC secteurhdc
                                WITH sectuser.secteurhdc_id = secteurhdc.id
                                WHERE sectuser.user_id = ".$user->getId());

            $result = $q->getResult();
            if ( count($result) ) {
                foreach( $result  as $row) {
                    $list[]=$row["idsecteur"];
                }
            }
        }
        return $list;
    }

    public function findSpecialiteByVille($criterea = false) {

        if ($criterea) {
            $where = "WHERE prospect.ville_id IN (".implode(",",$criterea["ville_id"]).")";
            if (isset($criterea["titre_id"]))
                $where .= "AND prospect.titre_id IN (".implode(",",$criterea["titre_id"]).")";
        } else
            $where="";

        $q = $this->getEntityManager()
            ->createQuery("SELECT DISTINCT spec.id as specid, spec.name as specname
                               FROM HdcOfficeBundle:Prospect prospect
                               INNER JOIN HdcOfficeBundle:Specialite spec
                                  WITH prospect.specialite_id = spec.id
                                ".$where." ORDER BY specname ASC");

        $result = $q->getResult();

        return $result;
    }

    public function findSpecialiteByBrickIMS($criterea = false) {

        if ($criterea) {
            $where = "WHERE prospect.brickims_id IN (".implode(",",$criterea["brickims_id"]).")";
            if (isset($criterea["titre_id"]))
                $where .= "AND prospect.titre_id IN (".implode(",",$criterea["titre_id"]).")";
        } else
            $where="";

        $q = $this->getEntityManager()
            ->createQuery("SELECT DISTINCT spec.id as specid, spec.name as specname
                               FROM HdcOfficeBundle:Prospect prospect
                               INNER JOIN HdcOfficeBundle:Specialite spec
                                  WITH prospect.specialite_id = spec.id
                                ".$where." ORDER BY specname ASC");

        $result = $q->getResult();

        return $result;
    }

    /**
     * @param null $critere
     * @param null $orderBy
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getProspect($critere = null, $orderBy = null, $limit=100, $offset = 0)
    {
        $qb = $this->createQueryBuilder("p");
        $qb->select('p');
        $this->getProspectBasicQueryBuilder($qb, $critere, $orderBy, $limit, $offset);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param null $critere
     * @return mixed
     */
    public function getCountProspect($critere = null)
    {
        $qb = $this->createQueryBuilder("p");
        $qb->select('count(p.id)');
        $this->getProspectBasicQueryBuilder($qb, $critere);

        return $qb->getQuery()->getSingleScalarResult();
    }


    function getProspectBasicQueryBuilder($qb, $critere = null, $orderBy = null, $limit = null, $offset = null){
        $aliases = [];
        if (!empty($critere)) {
            foreach ($critere as $field => $values){
                $entity = "p";
                $aliases[] = $entity;
                if( in_array($field, ["brickims_id", 'gouvernorat_id', 'region_id'])) {
                    if(!in_array("v", $aliases)) {
                        $qb
                            ->leftJoin("{$entity}.ville_id", 'v');
                    }
                    $entity = "v";
                    $aliases[] = $entity;
                    if(!in_array("g", $aliases) && $field == 'region_id') {
                        $qb->leftJoin("v.gouvernorat_id", 'g');
                        $entity = "g";
                        $aliases[] = $entity;
                    }
                }
                $qb->andWhere($qb->expr()->in("{$entity}.{$field}",$values));
            }
        }
        if($orderBy) {
            if(is_array($orderBy))
                foreach ($orderBy as $orderField => $orderDir) {
                    $entity = "p";
                    if($orderField == "brickims_id") {
                        $entity = "v";
                    }
                    $qb->addOrderBy("{$entity}.{$orderField}" , $orderDir);
                }
            else
                $qb->orderBy($orderBy);
        }
        if($limit)
            $qb->setMaxResults($limit);

        if($offset)
            $qb->setFirstResult($offset);

    }

    public function getFreePrivateProspect($specialiteId = null, $isCount=false, $offset = 0, $limit = 100)
    {
        $queryBuilder  = $this->_em->createQueryBuilder();
        $titreId = 9 ; // private
        $specialiteQuery = null;


        // select used brickims
        $queryBuilderUsedBrikSpec = $this->_em->createQueryBuilder();

        $queryBuilderUsedBrikSpec= $queryBuilderUsedBrikSpec->select('count (sd.brickims)')
            ->from('HdcOfficeBundle:SecteurDetail', 'sd')
            ->where($queryBuilder->expr()->isNotNull('sd.brickims'))
            ->andWhere($queryBuilder->expr()->isNull('sd.ville_id'));

        // select used ville
        $queryBuilderUsedVilleSpec = $this->_em->createQueryBuilder();

        $queryBuilderUsedVilleSpec= $queryBuilderUsedVilleSpec->select('count (sd2.ville_id)')
            ->from('HdcOfficeBundle:SecteurDetail', 'sd2')
            ->where($queryBuilder->expr()->isNotNull('sd2.ville_id'));

        $queryBuilder->select("prospect")
            ->from('HdcOfficeBundle:prospect', 'prospect')
            ->where($queryBuilder->expr()->eq('prospect.titre_id', $titreId));



        if($specialiteId) {
            $queryBuilderUsedBrikSpec->andWhere($queryBuilder->expr()->in('sd.specialite', $specialiteId));
            $queryBuilderUsedVilleSpec->andWhere($queryBuilder->expr()->in('sd2.specialite', $specialiteId));
            $queryBuilder->andWhere($queryBuilder->expr()->in('prospect.specialite_id', $specialiteId));

        }
        // to check if we have a brickims
        $countBrick = $queryBuilderUsedBrikSpec->getQuery()->getSingleScalarResult();

        // to check if we have a ville
        $countVille = $queryBuilderUsedVilleSpec->getQuery()->getSingleScalarResult();

        if($countBrick>0) {
            $queryBuilderUsedBrikSpec->select('IDENTITY (sd.brickims)');
            $queryBuilder->andWhere($queryBuilder->expr()->notIn('prospect.ville_id',
                $queryBuilderUsedBrikSpec->getDQL()));
        }

        if($countVille>0) {
            $queryBuilderUsedVilleSpec->select('IDENTITY (sd2.ville_id)');
            $queryBuilder->andWhere($queryBuilder->expr()->notIn('prospect.ville_id',
                $queryBuilderUsedVilleSpec->getDQL()));
        }

        if($isCount){
            $return = $queryBuilder->select("count (prospect.id)")->getQuery()->getSingleScalarResult();
        }
        else{
            $return = $queryBuilder
                ->orderBy('prospect.lastname', 'ASC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
        }

        return $return;
    }

}

