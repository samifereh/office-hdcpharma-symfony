<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class Commande extends EntityRepository
{
    public function findAll($userID = false)
    {
        $q = $this->getEntityManager()
            ->createQuery("SELECT orders, prospect.id, prospect.lastname pro_lastname, prospect.firstname pro_firstname, u.id, u.firstname, u.lastname
                               FROM HdcOfficeBundle:Commande orders
                               INNER JOIN HdcOfficeBundle:Prospect prospect
                                  WITH orders.prospect_id = prospect.id
                               INNER JOIN HdcOfficeBundle:User u
                                  WITH orders.user_id = u.id
                               ".(($userID)?" WHERE u.id = ".$userID:""));
        return $q->getResult();
    }

    public function findInfoOneBy(array $criteria)
    {
        $q = $this->getEntityManager()
            ->createQuery("SELECT orders,
                                    prospect.id, prospect.lastname as pro_lastname, prospect.firstname as pro_firstname,
                                    u.id, u.firstname, u.lastname,
                                    fournisseur.id as fid, fournisseur.lastname four_lastname, fournisseur.firstname four_firstname
                               FROM HdcOfficeBundle:Commande orders
                               INNER JOIN HdcOfficeBundle:Prospect prospect
                                  WITH orders.prospect_id = prospect.id
                               INNER JOIN HdcOfficeBundle:User u
                                  WITH orders.user_id = u.id
                               LEFT JOIN HdcOfficeBundle:Prospect fournisseur
                                  WITH orders.fournisseur_id = fournisseur.id
                               WHERE orders.id = ".$criteria["id"]);
        return $q->getOneOrNullResult();
    }
}

