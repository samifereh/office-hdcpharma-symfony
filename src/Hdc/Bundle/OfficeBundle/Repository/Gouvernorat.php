<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class Gouvernorat extends EntityRepository
{
    public function getAll()
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT g, r.id, r.name
                               FROM HdcOfficeBundle:Gouvernorat g
                               INNER JOIN HdcOfficeBundle:Region r
                                  WITH g.region_id = r.id
                               ");
        return $q->getResult();
    }
}

