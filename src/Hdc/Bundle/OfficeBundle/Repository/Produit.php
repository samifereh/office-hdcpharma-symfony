<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class Produit extends EntityRepository
{
    public function findAll()
    {
        $q = $this->getEntityManager()
            ->createQuery("SELECT prod, grp.id, grp.name as grpname, tva.id, tva.tauxtva as tauxtva
                               FROM HdcOfficeBundle:Produit prod
                               LEFT JOIN HdcOfficeBundle:GroupeProduit grp
                                  WITH prod.groupeproduit_id = grp.id
                               LEFT JOIN HdcOfficeBundle:TauxTva tva
                                  WITH prod.tauxtva_id = tva.id
                               ");
        return $q->getResult();
    }

    public function findFreeForGamme()
    {
        $queryBuilder  = $this->_em->createQueryBuilder();

        $queryBuilderUsedProd = $this->_em->createQueryBuilder();
        $queryBuilderUsedProd->select('IDENTITY (gp.produit_id)')
            ->from('HdcOfficeBundle:GammeProduits', 'gp');

        return $queryBuilder->select("prod")
            ->from('HdcOfficeBundle:Produit', 'prod')
            ->where($queryBuilder->expr()->notIn('prod.id',  $queryBuilderUsedProd->getDQL()))
            ->getQuery()
            ->getResult();

    }

    public function search($searchKey="%")
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT prd
                               FROM HdcOfficeBundle:Produit prd
                                  WHERE prd.name like '" . $searchKey . "'
                               ");
        return $q->getResult();
    }
}

