<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BonGratuiteDetail extends EntityRepository
{

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT d, p.id, p.name
                               FROM HdcOfficeBundle:BonGratuiteDetail d
                               INNER JOIN HdcOfficeBundle:Produit p
                                  WITH d.produit = p.id
                                  WHERE d.bongratuite = ".$criteria["bongratuite"]
            );
        return $q->getResult();
    }
}

