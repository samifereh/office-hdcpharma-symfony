<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class Specialite extends EntityRepository
{

    public function findFreeForGamme()
    {
        $queryBuilder  = $this->_em->createQueryBuilder();

        $queryBuilderUsedSpe = $this->_em->createQueryBuilder();

        $queryBuilderUsedSpe= $queryBuilderUsedSpe->select('IDENTITY (gs.specialite_id)')
            ->from('HdcOfficeBundle:GammeSpecialites', 'gs');

        return $queryBuilder->select("specialite")
            ->from('HdcOfficeBundle:Specialite', 'specialite')
            ->where($queryBuilder->expr()->notIn('specialite.id',  $queryBuilderUsedSpe->getDQL()))
            ->getQuery()
            ->getResult();

    }

}

