<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SecteurHDC extends EntityRepository
{
    function deleteByGammeId($gammeId){
        $queryBuilder  = $this->_em->createQueryBuilder("s");
        $result = $queryBuilder->delete($this->_entityName, "s")
            ->where($queryBuilder->expr()->eq("s.gamme_id",  $gammeId))
            ->getQuery()
            ->execute();

        return $result;
    }

    function deleteRelationUser($user){
        $queryBuilder  = $this->_em->createQueryBuilder("s");
        $result = $queryBuilder->update($this->_entityName, "s")
            ->set('s.user', 'NULL')
            ->where($queryBuilder->expr()->eq("s.user",  $user))
            ->getQuery()
            ->execute();

        return $result;
    }

}

