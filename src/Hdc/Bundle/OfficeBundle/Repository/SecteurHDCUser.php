<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SecteurHDCUser extends EntityRepository
{

    public function findAll()
    {
        $q = $this->getEntityManager()
            ->createQuery("SELECT su, u.id, u.lastname, u.firstname,
                                  se.id as seid, se.name as sename
                               FROM HdcOfficeBundle:SecteurHDCUser su
                               INNER JOIN HdcOfficeBundle:User u
                                  WITH su.user_id = u.id
                               INNER JOIN HdcOfficeBundle:SecteurHDC se
                                  WITH su.secteurhdc_id = se.id
                               ");
        return $q->getResult();
    }

    public function findByUser(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT s, secteur.id as secteurid, secteur.name as secteurname
                               FROM HdcOfficeBundle:SecteurHDCUser s
                               INNER JOIN HdcOfficeBundle:SecteurHDC secteur
                                  WITH s.secteurhdc_id = secteur.id
                                  WHERE s.user_id = ".$criteria["user_id"]
            );
        return $q->getResult();
    }

}

