<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GammeProduits extends EntityRepository
{
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT s, v.id, v.name
                               FROM HdcOfficeBundle:GammeProduits s
                               INNER JOIN HdcOfficeBundle:Produit v
                                  WITH s.produit_id = v.id
                                  WHERE s.gamme_id = ".$criteria["gamme_id"]
            );
        return $q->getResult();
    }
}

