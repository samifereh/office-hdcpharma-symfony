<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GammeSpecialites extends EntityRepository
{
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT s, v.id, v.name
                               FROM HdcOfficeBundle:GammeSpecialites s
                               INNER JOIN HdcOfficeBundle:Specialite v
                                  WITH s.specialite_id = v.id
                                  WHERE s.gamme_id = ".$criteria["gamme_id"]
            );
        return $q->getResult();
    }

    public function findSpecialitesByGammes(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT s, v.id, v.name
                               FROM HdcOfficeBundle:GammeSpecialites s
                               INNER JOIN HdcOfficeBundle:Specialite v
                                  WITH s.specialite_id = v.id
                                  WHERE s.gamme_id in ( ".implode(',',$criteria["gamme_id"]).")"
            );
        return $q->getResult();
    }
}

