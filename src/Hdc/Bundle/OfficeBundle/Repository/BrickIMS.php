<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BrickIMS extends EntityRepository
{

    public function findFree($specialityIds, array $gouvList = null)
    {
        $queryBuilder  = $this->_em->createQueryBuilder();

        $queryBuilderUsedBrikSpec = $this->_em->createQueryBuilder();

        $queryBuilderUsedBrikSpec= $queryBuilderUsedBrikSpec->select('count (sd.brickims)')
            ->from('HdcOfficeBundle:SecteurDetail', 'sd')
            ->where($queryBuilder->expr()->in('sd.specialite',  $specialityIds))
            ->andWhere($queryBuilder->expr()->isNotNull('sd.brickims'))
            ->andWhere($queryBuilder->expr()->isNull('sd.ville_id'));

        // to check if we have a brickims
        $count = $queryBuilderUsedBrikSpec->getQuery()->getSingleScalarResult();

        $queryBuilder->select("brickims")
            ->from('HdcOfficeBundle:BrickIMS', 'brickims');

        if($count>0) {
            $queryBuilderUsedBrikSpec->select('IDENTITY (sd.brickims)');
            $queryBuilder->where($queryBuilder->expr()->notIn('brickims.id', $queryBuilderUsedBrikSpec->getDQL()));
        }
        if($gouvList) {
            $queryBuilder->andWhere($queryBuilder->expr()->in('brickims.gouvernorat_id', $gouvList));
        }

        $return = $queryBuilder
            ->orderBy('brickims.name', 'ASC')
            ->getQuery();
        return $return->getResult();

    }

    public function isVilleUsedInSecteurs(array $gamme_list,$ville_id)
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT detail FROM HdcOfficeBundle:SecteurDetail detail
                                  WHERE detail.gamme_id IN (:ids) AND detail.ville_id = $ville_id
                               ")
            ->setParameters(array('ids'=> $gamme_list));

        $ret = $q->getResult();


        if (count($ret))
            return true;
        else
            return false;

        //if $q->getResult();
    }
}

