<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SecteurIMS extends EntityRepository
{
    public function findAll()
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT s, v.id, v.name
                               FROM HdcOfficeBundle:SecteurIMS s
                               INNER JOIN HdcOfficeBundle:Ville v
                                  WITH s.ville_id = v.id
                               ");
        return $q->getResult();
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT s, v.id, v.name
                               FROM HdcOfficeBundle:SecteurIMS s
                               INNER JOIN HdcOfficeBundle:Ville v
                                  WITH s.ville_id = v.id
                                  WHERE s.brickims_id = ".$criteria["brickims_id"]
                               );
        return $q->getResult();
    }
}

