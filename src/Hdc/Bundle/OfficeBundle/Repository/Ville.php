<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class Ville extends EntityRepository
{
    public function getAll()
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT v, g.id, g.name, b.name as brickname
                               FROM HdcOfficeBundle:Ville v
                               INNER JOIN HdcOfficeBundle:Gouvernorat g
                                  WITH v.gouvernorat_id = g.id
                                   LEFT JOIN HdcOfficeBundle:BrickIMS b
                                  WITH v.brickims_id = b.id
                               ");
        return $q->getResult();
    }


    public function findFree($specialityIds, array $brickIMS = null)
    {
        $queryBuilder  = $this->_em->createQueryBuilder();

        $queryBuilderUsedVilleSpec = $this->_em->createQueryBuilder();

        $queryBuilderUsedVilleSpec= $queryBuilderUsedVilleSpec->select('count (sd.ville_id)')
            ->from('HdcOfficeBundle:SecteurDetail', 'sd')
            ->where($queryBuilder->expr()->in('sd.specialite',  $specialityIds))
            ->andWhere($queryBuilder->expr()->isNotNull('sd.ville_id'));

        // to check if we have a brickims
        $count = $queryBuilderUsedVilleSpec->getQuery()->getSingleScalarResult();

        $queryBuilder->select("ville")
            ->from('HdcOfficeBundle:Ville', 'ville');

        if($count>0) {
            $queryBuilderUsedVilleSpec->select('IDENTITY (sd.ville_id)');
            $queryBuilder->where($queryBuilder->expr()->notIn('ville.id',  $queryBuilderUsedVilleSpec->getDQL()));
        }
        if($brickIMS) {
            $queryBuilder->andWhere($queryBuilder->expr()->in('ville.brickims_id', $brickIMS));
        }
        return $queryBuilder->orderBy("ville.name", "ASC")
            ->getQuery()
            ->getResult();

    }

    public function findForSpecialite(array $specialite ,array $titreId = null ,array $brickIMS = null )
    {
        $queryBuilder  = $this->_em->createQueryBuilder();
        $queryBuilder->select("ville")
            ->from('HdcOfficeBundle:Ville', 'ville');

        $queryBuilderUsedPros = $this->_em->createQueryBuilder();



        $queryBuilderUsedPros = $queryBuilderUsedPros->select('IDENTITY (psp.ville_id)')
            ->from('HdcOfficeBundle:Prospect', 'psp');
        if($brickIMS) {
            $queryBuilderUsedPros->where($queryBuilderUsedPros->expr()->in('psp.brickims_id', $brickIMS));
            $queryBuilder->where($queryBuilder->expr()->in('ville.brickims_id', $brickIMS));
        }

        if($titreId)
            $queryBuilderUsedPros->andWhere($queryBuilderUsedPros->expr()->in('psp.titre_id', $titreId));

        if($specialite)
            $queryBuilderUsedPros->andWhere($queryBuilderUsedPros->expr()->in('psp.specialite_id', $specialite));


        $queryBuilder->andWhere($queryBuilder->expr()->in('ville.id', $queryBuilderUsedPros->getDQL()))
            ->orderBy("ville.name");
        return $queryBuilder->getQuery()
            ->getResult();

    }

    public function isVilleUsedInSecteurs(array $gamme_list,$ville_id)
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT detail FROM HdcOfficeBundle:SecteurDetail detail
                                  WHERE detail.gamme_id IN (:ids) AND detail.ville_id = $ville_id
                               ")
        ->setParameters(array('ids'=> $gamme_list));

        $ret = $q->getResult();


        if (count($ret))
            return true;
        else
            return false;

        //if $q->getResult();
    }
}

