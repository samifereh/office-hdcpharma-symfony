<?php

namespace Hdc\Bundle\OfficeBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BonGratuite extends EntityRepository
{
    public function findAll()
    {

        $q = $this->getEntityManager()
            ->createQuery("SELECT bons, user.id, user.firstname, user.lastname, user.username
                               FROM HdcOfficeBundle:BonGratuite bons
                               INNER JOIN HdcOfficeBundle:User user
                                  WITH bons.user_id = user.id
                               ");
        return $q->getResult();
    }
}

