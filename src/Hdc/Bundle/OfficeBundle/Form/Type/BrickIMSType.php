<?php

namespace Hdc\Bundle\OfficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BrickIMSType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('gouvernorat_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Gouvernorat',
                'query_builder' => function ($repository)
                {return $repository->createQueryBuilder('s')->orderBy("s.name", "ASC");},
                'choice_label' => "name",
                'placeholder' => 'Sélectionner un gouvernorat',
                'required'    => true,
                'empty_data' => null
            ))
            ->add('save', 'submit');
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'brickims';
    }
}