<?php

namespace Hdc\Bundle\OfficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('palier_gratuite', 'integer')
            ->add('prix_achatht', 'money', array('required'=> false,"currency"=>"TND", "scale"=>3))
            ->add('prix_revientht', 'money', array('required'=> false,"currency"=>"TND", "scale"=>3))
            ->add('prix_venteht', "money", array("currency"=>"TND", "scale"=>3))
            ->add('tauxtva_id', 'entity', array(
                'class' => 'HdcOfficeBundle:TauxTva',
                'choice_label' => "tauxtva"
            ))
            ->add('groupeproduit_id', 'entity', array(
                'class' => 'HdcOfficeBundle:GroupeProduit',
                'choice_label' => "name",
                'required'    => false,
                'placeholder' => 'Aucun groupe, choisir un groupe de produit',
                'empty_data' => null
            ))
            ->add('save', 'submit');
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'brickims';
    }
}