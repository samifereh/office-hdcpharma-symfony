<?php

namespace Hdc\Bundle\OfficeBundle\Form\Type;

use Hdc\Bundle\OfficeBundle\Form\EventListner\IsAjaxFieldSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProspectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('titre_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Titre',
                'query_builder' => function ($repository)
                {return $repository->createQueryBuilder('s')->orderBy("s.name", "ASC");},
                'choice_label' => "name",
                'required'    => true,
                'placeholder' => 'Aucun ou choisir un titre',
                'empty_data' => null
            ))
            ->add('firstname', 'text', array('required'=>false))
            ->add('lastname', 'text',array('required'    => true))
            ->add('adresse', 'textarea', array('required'=>true))
            ->add('telephone1', 'text', array('required'=>false))
            ->add('telephone2', 'text', array('required'=>false))
            ->add('email', 'text', array('required'=>false))
            ->add('remarque', 'textarea', array('required'=>false))
            ->add('ville_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Ville',
                'query_builder' => function ($repository) use ($options)
                {
                    $qb = $repository->createQueryBuilder('s');
                    if($options['data'] && $options['data']->getVilleId() && $options['data']->getVilleId()
                            ->getBrickimsId()){
                        $qb->andWhere($qb->expr()->eq("s.brickims_id", ":brickims"))
                            ->setParameter("brickims", $options['data']->getVilleId()->getBrickimsId()->getId());
                    }
                    $qb->setMaxResults(100);
                    return $qb->orderBy("s.name", "ASC");
                },
                'choice_label' => "name",
                'required'    => true,
            ))
            ->add('specialite_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Specialite',
                'query_builder' => function ($repository)
                {return $repository->createQueryBuilder('s')->orderBy("s.name", "ASC");},
                'choice_label' => "name",
                'required'    => true,
            ))
            ->add('genre_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Genre',
                'query_builder' => function ($repository)
                {return $repository->createQueryBuilder('s')->orderBy("s.name", "ASC");},
                'choice_label' => "name",
                'required'    => true,
                'placeholder' => 'Aucun ou choisir un Genre',
                'empty_data' => null
            ))
            ->add('cursus_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Cursus',
                'query_builder' => function ($repository)
                {return $repository->createQueryBuilder('s')->orderBy("s.name", "ASC");},
                'choice_label' => "name",
                'required'    => true,
                'placeholder' => 'Aucun ou choisir un cursus',
                'empty_data' => null
            ))

            ->add('potentiel_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Potentiel',
                'query_builder' => function ($repository)
                {return $repository->createQueryBuilder('s')->orderBy("s.name", "ASC");},
                'choice_label' => "name",
                'required'    => true,
                'placeholder' => 'Aucun ou choisir un potentiel',
                'empty_data' => null
            ))
            ->add('rps', 'textarea', array('required'=>false))
            ->add('structure', 'textarea', array('required'=>false))
            ->add('lat', 'text', array('required'=>false))
            ->add('lng', 'text', array('required'=>false))
            ->add('save', 'submit');


        $subscriber = new IsAjaxFieldSubscriber($builder->getFormFactory());
        $builder->addEventSubscriber($subscriber);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'prospect';
    }
}