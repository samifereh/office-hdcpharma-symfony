<?php

namespace Hdc\Bundle\OfficeBundle\Form\Type;

use Hdc\Bundle\OfficeBundle\Repository\Prospect;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CommandeType extends AbstractType
{
    var $userManager;

    public function __construct($userManager) {
        $this->userManager = $userManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date','dateTimePicker')
            ->add('type', "choice", array(
                'choices' => array('Commande' => 1, 'Avoir' => -1),
                'choices_as_values' => true))
            ->add('reference', 'text')
            ->add('prospect_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Prospect',
                'query_builder' => function ($repository)
                    {return $repository->createQueryBuilderHDC('s', $this->userManager)->orderBy("s.lastname", "ASC");},
                'choice_label' => "completeName",
            ))
            ->add('fournisseur_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Prospect',
                'query_builder' => function (Prospect $repository)
                {return $repository->createQueryBuilderHDC('s', $this->userManager, true);},
                'choice_label' => "completeName",
                'required'    => false,
                'placeholder' => 'commande directe ou choisir un fournisseur',
                'empty_data' => null
            ))
            ->add('submit', 'submit');
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'commande';
    }
}