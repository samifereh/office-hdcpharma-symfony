<?php

namespace Hdc\Bundle\OfficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VisiteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        for($i = 1; $i <= 8; $i++) {
            $palierAttente[$i*15] = $i*15;
        }
        for($i = 1; $i <= 12; $i++) {
            $paliervisite[$i*5] = $i*5;
        }

        $builder
            ->add('datevisite','dateTimePicker')
            //->add('datesaisie','dateTimePicker')
            ->add('tempsattente', "choice", array(
                'choices' => $palierAttente,
                'choices_as_values' => true))
            ->add('tempsvisite', "choice", array(
                'choices' => $paliervisite,
                'choices_as_values' => true))
            ->add('remarque')
            ->add('rp')
            ->add('concurrent')
            ->add('acces', "choice", array(
                'choices' => array("Rapide"=>0,"Lent"=>1),
                'choices_as_values' => true))
            ->add('submit', 'submit', array('label' => 'Create'))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hdc\Bundle\OfficeBundle\Entity\Visite'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hdc_bundle_officebundle_visite';
    }
}
