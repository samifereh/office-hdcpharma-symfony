<?php

namespace Hdc\Bundle\OfficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BonGratuiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date','dateTimePicker')
            ->add('type', "choice", array(
                'choices' => array('Entrée' => 1, 'Sortie' => -1),
                'choices_as_values' => true))
            ->add('reference', 'text')
            ->add('user_id', 'entity', array(
                'class' => 'HdcOfficeBundle:User',
                'choice_label' => "username",
            ))
            ->add('save', 'submit');
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'bongratuite';
    }
}