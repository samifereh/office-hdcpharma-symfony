<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class UserController extends Controller
{
    /**
     * @Route("/user/", name="userlist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:User');


        $objects = $repository->findAll();
        return array('objects'=>$objects);
    }

    /**
     * @Route("/user/add", name="useradd")
     * @Template()
     */
    public function addAction(Request $request)
    {

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $form = $formFactory->createForm()
            ->add('firstname', 'text', array("required"=>true))
            ->add('lastname', 'text', array("required"=>true))
            ->add('tel1', 'text', array('required'=>true))
            ->add('tel2', 'text', array('required'=>false))
            ->add('adresse', 'textarea', array('required'=>true))
            ->add('save', 'submit');
        $form->setData($user);


        $form->handleRequest($request);

        if ($form->isValid()) {

            $donnee = $form->getData();
            $password = $donnee->getPassword();
            //var_dump($password);die('Hello');
            $hash = password_hash($password,PASSWORD_BCRYPT,['cost' => 13]) ;
            $user->setPassword($hash);

            $userManager->updateUser($user);

            return $this->redirect($this->generateUrl('userlist'));
        }

        return array('form' => $form->createView(), 'mode' => "Ajouter");
    }

    /**
     * @Route("/user/edit/{id}", name="useredit")
     * @Template()
     */
    public function editAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:User');


        $user = $repository->findOneBy(array('id'=> $id));

        if ( is_object($user) && in_array("ROLE_SUPER_ADMIN",$user->getRoles()) )
            die('not allowed');

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $form = $formFactory->createForm()
            ->add('firstname', 'text', array("required"=>false))
            ->add('lastname', 'text')
            ->add('tel1', 'text', array('required'=>false))
            ->add('tel2', 'text', array('required'=>false))
            ->add('adresse', 'textarea', array('required'=>true))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ))
            ->add('submit', 'submit');

        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $donnee = $form->getData();
            $password = $donnee->getPassword();
            //var_dump($password);die('Hello');
            $hash = password_hash($password,PASSWORD_BCRYPT,['cost' => 13]) ;
            $user->setPassword($hash);

            //var_dump($password);die('Hello');

            $userManager->updateUser($user);

            return $this->redirect($this->generateUrl('userlist'));
        }

        return array('form' => $form->createView(), 'mode' => "Editer",);
    }

    /**
     * @Route("/user/delete/{id}", name="userdelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:User');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if ( is_object($oModel) && in_array("ROLE_SUPER_ADMIN",$oModel->getRoles()) )
            die('not allowed');

        if (!$oModel) {
            throw $this->createNotFoundException("Aucun utilisateur trouvé avec cet identifiant");
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();

        $this->addFlash(
            'notice',
            'Utilisteur supprimé!'
        );

        return $this->redirect($this->generateUrl('userlist'));

    }

    /**
     * @Route("/user/status/{id}", name="userstatus")
     */
    public function statusAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:User');


        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');


        $user = $repository->findOneBy(array('id'=> $id));

        if (!$user) {
            throw $this->createNotFoundException("Pas d'utilisateur trouvé avec cet identifiant");
        }

        if ( $user->isEnabled() )
            $user->setEnabled(false);
        else
            $user->setEnabled(true);


        $userManager->updateUser($user);

        $this->addFlash(
            'notice',
            'Status changé!'
        );

        return $this->redirect($this->generateUrl('userlist'));

    }

}
