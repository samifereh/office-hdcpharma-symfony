<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Potentiel;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class PotentielController extends Controller
{
    /**
     * @Route("/potentiel/", name="potentiellist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Potentiel');


        $objects = $repository->findAll();
        return array('objects'=>$objects);
    }

    /**
     * @Route("/potentiel/add", name="potentieladd")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function addAction(Request $request)
    {

        $oModel = new Potentiel();

        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();

            $this->addFlash(
                'success',
                'Potentiel enregistré!'
            );

            return $this->redirect($this->generateUrl('potentiellist'));
        }

        return array('title'=>'Ajouter un potentiel', 'mode'=>'Ajouter', 'form' => $form->createView());
    }


    /**
     * @Route("/potentiel/edit/{id}", name="potentieledit")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Potentiel');


        $oModel = $repository->findOneBy(array('id'=> $id));



        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();

            $this->addFlash(
                'success',
                'Potentiel enregistré!'
            );

            return $this->redirect($this->generateUrl('potentiellist'));
        }

        return array('title'=>'Editer un potentiel', 'mode'=>'Editer', 'form' => $form->createView());
    }

    /**
     * @Route("/potentiel/delete/{id}", name="potentieldelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Potentiel');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de potentiel');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();

        $this->addFlash(
            'success',
            'Potentiel supprimé!'
        );

        return $this->redirect($this->generateUrl('potentiellist'));

    }
}
