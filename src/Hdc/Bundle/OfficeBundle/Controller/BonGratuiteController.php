<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Hdc\Bundle\OfficeBundle\Entity\BonGratuiteDetail;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\BonGratuite;
use Hdc\Bundle\OfficeBundle\Form\Type\BonGratuiteType;
use Hdc\Bundle\OfficeBundle\Entity\Produit;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class BonGratuiteController extends Controller
{
    /**
     * @Route("/gratuite/", name="bongratuitelist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:BonGratuite');

        $oRows = $repository->findAll();
        return array('oRows'=>$oRows);
    }

    /**
     * @Route("/gratuite/add", name="bongratuiteadd")
     * @Template("@HdcOffice/BonGratuite/addedit.html.twig")
     */
    public function addAction(Request $request)
    {


        $oPersist = new BonGratuite();

        // get all products
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Produit');
        $oProducts = $repository->search();

        $form = $this->createForm(new BonGratuiteType(), $oPersist);
        $form->handleRequest($request);

        // TODO test grid

        if ( $form->isValid() ) {
            $formdata = $form->getData();

            $oPersist->setDate($formdata->getDate());
            $oPersist->setType($formdata->getType());
            $oPersist->setReference($formdata->getReference());
            $oPersist->setUserId($formdata->getUserId());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oPersist);
            $em->flush();

            if (count($request->request->get("produit")))
            {
                foreach ($request->request->get("produit") as $key => $produitid)
                {

                    $aQte = $request->request->get("quantite");
                    $oPersistDt = new BonGratuiteDetail();
                    $oPersistDt->setBonId($oPersist);
                    $oPersistDt->setProduitId($repository->find($produitid));
                    $oPersistDt->setQuantity($aQte[$key]);
                    $em->persist($oPersistDt);
                }
            }

            $em->flush();

            $this->addFlash(
                'success',
                'Bon de sortie enregistré!'
            );

            return $this->redirect($this->generateUrl('bongratuitelist'));
        }

        return array('mode'=>"Ajouter", 'form' => $form->createView(), "oProducts"=>$oProducts);
    }

    /**
     * @Route("/gratuite/edit/{id}", name="bongratuiteedit")
     * @Template("@HdcOffice/BonGratuite/addedit.html.twig")
     */
    public function editAction(Request $request, $id)
    {

        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:BonGratuite');

        $oPersist = $repository->findOneBy(array('id'=> $id));

        $repo2 = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:BonGratuiteDetail');
        $details = $repo2->findBy(array("bongratuite"=>$id));

        // get all products
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Produit');
        $oProducts = $repository->search();

        $form = $this->createForm(new BonGratuiteType(), $oPersist);
        $form->handleRequest($request);


        if ( $form->isValid() ) {
            $formdata = $form->getData();

            $oPersist->setDate($formdata->getDate());
            $oPersist->setType($formdata->getType());
            $oPersist->setReference($formdata->getReference());
            $oPersist->setUserId($formdata->getUserId());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oPersist);
            $em->flush();

            if (count($request->request->get("produit")))
            {
                $q = $em->createQuery('delete from HdcOfficeBundle:BonGratuiteDetail tb where tb.bongratuite = '.$id);
                $numDeleted = $q->execute();
                foreach ($request->request->get("produit") as $key => $produitid)
                {

                    $aQte = $request->request->get("quantite");
                    $oPersistDt = new BonGratuiteDetail();
                    $oPersistDt->setBonId($oPersist);
                    $oPersistDt->setProduitId($repository->find($produitid));
                    $oPersistDt->setQuantity($aQte[$key]);
                    $em->persist($oPersistDt);
                }
            }

            $em->flush();

            $this->addFlash(
                'success',
                'Bon de sortie enregistré!'
            );

            // $this->addFlash is equivalent to $this->get('session')->getFlashBag()->add


            return $this->redirect($this->generateUrl('bongratuitelist'));
        }

        return array('mode'=>"Ajouter", 'form' => $form->createView(), "oProducts"=>$oProducts, "aProductsSaved"=>$details);
    }

    /**
     * @Route("/gratuite/delete/{id}", name="gratuitedelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:BonGratuite');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de bon trouvé');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();

        $this->addFlash(
            'success',
            'Secteur HDC supprimé!'
        );


        return $this->redirect($this->generateUrl('bongratuitelist'));

    }

}
