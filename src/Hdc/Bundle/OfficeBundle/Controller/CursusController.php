<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Cursus;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class CursusController extends Controller
{
    /**
     * @Route("/cursus/", name="cursuslist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Cursus');


        $objects = $repository->findAll();
        return array('objects'=>$objects);
    }

    /**
     * @Route("/cursus/add", name="cursusadd")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function addAction(Request $request)
    {

        $oModel = new Cursus();

        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();
            return $this->redirect($this->generateUrl('cursuslist'));
        }

        return array('title'=>'Ajouter un cursus', 'mode'=>'Ajouter', 'form' => $form->createView());
    }


    /**
     * @Route("/cursus/edit/{id}", name="cursusedit")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Cursus');


        $oModel = $repository->findOneBy(array('id'=> $id));



        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();
            return $this->redirect($this->generateUrl('cursuslist'));
        }

        return array('title'=>'Editer un cursus', 'mode'=>'Editer', 'form' => $form->createView());
    }

    /**
     * @Route("/cursus/delete/{id}", name="cursusdelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Cursus');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de Cursus');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();


        return $this->redirect($this->generateUrl('cursuslist'));

    }
}
