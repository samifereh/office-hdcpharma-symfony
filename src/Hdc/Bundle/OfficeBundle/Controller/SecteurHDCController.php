<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Hdc\Bundle\OfficeBundle\Entity\Gouvernorat;
use Hdc\Bundle\OfficeBundle\Entity\SecteurDetail;
use Hdc\Bundle\OfficeBundle\Entity\SecteurHDCDetail;
use Hdc\Bundle\OfficeBundle\Entity\Gamme;
use Hdc\Bundle\OfficeBundle\Entity\Prospect;
use Hdc\Bundle\OfficeBundle\Form\Type\SecteurHDCType;
use Hdc\Bundle\OfficeBundle\Repository\Ville;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\SecteurHDC;
use Hdc\Bundle\OfficeBundle\Entity\SecteurHDCVille;
use Hdc\Bundle\OfficeBundle\Entity\SecteurHDCSpecialite;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class SecteurHDCController extends Controller
{
    /**
     * @Route("/secteurhdc/", name="secteurhdclist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:SecteurHDC');


        $objects = $repository->findAll();
        return array('objects'=>$objects);
    }

    /**
     * @Route("/secteurhdc/view/{id}/", requirements={"id" = "\d+"}, name="secteurhdcview")
     * @ParamConverter("secteur", options={"mapping": {"id": "id" }})
     * @Template("@HdcOffice/SecteurHDC/view.html.twig")
     */
    public function viewAction(SecteurHDC $secteur)
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:SecteurHDC');


        if(is_null($secteur))
            return $this->redirect($this->generateUrl('gammelist'));

        $specialityList = [];
        $cityList = [];
        $brickimsList = [];

        foreach ($secteur->getDetails() as $detail){
            if($detail->getSpecialite()){
                $specialityList[$detail->getSpecialite()->getId()] = $detail->getSpecialite()->getName();
            }
            if($detail->getVilleId()){
                $cityList[$detail->getVilleId()->getId()] = $detail->getVilleId()->getName();
            }
            if($detail->getBrickims()){
                $brickimsList[$detail->getBrickims()->getId()] = $detail->getBrickims()->getName();
            }


        }

        return array(
            'object'=>$secteur,
            'specialityList' => $specialityList,
            'cityList' => $cityList,
            'brickimsList' => $brickimsList
        );
    }

    /**
     * @Route("/secteurhdc/add/{gammeId}", requirements={"gammeId" = "\d+"}, name="secteurhdcadd")
     * @Template("@HdcOffice/SecteurHDC/add.html.twig")
     */
    public function addAction(Request $request, $gammeId)
    {

        $user = $this->getUser();
        $gamme = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gamme')->find($gammeId);

        $oModel = new SecteurHDC();

        $form = $this->createForm(new SecteurHDCType(), $oModel);

        $form->handleRequest($request);

        if ( $form->isValid() ) {

            $formdata = $form->getData();

            $existSector = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:SecteurHDC')->findBy(['name' => $formdata->getName()]);

            if($existSector){
                $this->addFlash(
                    'warning',
                    'Ce nom du secteur est déjà pris!'
                );
            }else{

                $oModel->setName($formdata->getName());
                $oModel->setGammeId($gamme);
                $oModel->setUser($user);

                if (
                    (count($request->request->get("select_Ville")) && !in_array(0,$request->get("select_Ville") )) ||
                    (count($request->request->get("select_BrickIMS")) && !in_array(0,$request->get("select_BrickIMS") ))
                ) {
                    /* Persist the object to the database */
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($oModel);
                    $em->flush();

                    $specialites = $request->request->get("select_Specialite");
                    $specialiteList = [];
                    if(count($specialites) == 0 || in_array('0', $specialites))
                    {
                        foreach ($gamme->getSpecialites() as $specialite){
                            $specialiteList[] = $specialite->getSpecialiteId();
                        }
                    }
                    else{
                        foreach ($specialites as $specialiteId){
                            $specialiteList[] = $this->getDoctrine()
                                ->getRepository('HdcOfficeBundle:Specialite')->find($specialiteId);;
                        }
                    }

                    if (count($request->request->get("select_Ville"))) {
                        $repo = $this->getDoctrine()
                            ->getRepository('HdcOfficeBundle:Ville');


                        foreach ($request->request->get("select_Ville") as $key => $ville_id) {
                            $ville = $repo->find(array("id" => $ville_id));
                            foreach ($specialiteList as $spec) {
                                $oPersistDt = new SecteurDetail();
                                $oPersistDt->setSecteurHDCId($oModel);
                                $oPersistDt->setSpecialite($spec);
                                $oPersistDt->setVilleId($ville);
                                $oPersistDt->setBrickims($ville->getBrickimsId());
                                $em->persist($oPersistDt);
                            }
                        }
                    } elseif (count($request->request->get("select_BrickIMS"))) {
                        $repo = $this->getDoctrine()
                            ->getRepository('HdcOfficeBundle:BrickIMS');

                        foreach ($request->request->get("select_BrickIMS") as $key => $brickims_id) {
                            $brickims = $repo->find(array("id" => $brickims_id));
                            foreach ($specialiteList as $spec) {
                                $oPersistDt = new SecteurDetail();
                                $oPersistDt->setSecteurHDCId($oModel);
                                $oPersistDt->setSpecialite($spec);
                                $oPersistDt->setBrickims($brickims);
                                $em->persist($oPersistDt);
                            }
                        }
                    }

                    $em->flush();


                    $this->addFlash(
                        'success',
                        'Secteur HDC ajouté!'
                    );


                    return $this->redirect($this->generateUrl('gammeview', array('id' => $gammeId)));
                }else{
                    $this->addFlash(
                        'warning',
                        'Merci de choisir Brick IMS ou Ville!'
                    );
                }
            }
        }
        /* key = EntityName, value= label */
        $filtres = array(
            "Specialite"=>"Spécialités",
            "Region"=>"Régions",
            "Gouvernorat"=>"Gouvernorats",
            "BrickIMS"=>"Brick IMS",
            "Ville"=>"Localités"
        );

        $selectedSpecialites = array();
        foreach ($gamme->getSpecialites() as $specialite)
        {
            $selectedSpecialites[] = $specialite->getSpecialiteId()->getId();
        }

        $titreId = array(9);
        if(count($selectedSpecialites)>0)
        {
            $entity = "Specialite";
            $repo = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:'.$entity);
            $available_filtres[$entity]["label"] = $filtres[$entity];
            $available_filtres[$entity]["data"] = $repo->findBy(array('id'=> $selectedSpecialites));
            unset($filtres[$entity]);
        }

        foreach ($filtres as $entity => $label) {
            if (!in_array(['BrickIMS', 'Ville'], $filtres)) {
                $repo = $this->getDoctrine()
                    ->getRepository('HdcOfficeBundle:'.$entity);
                $available_filtres[$entity]["label"] = $label;
                $available_filtres[$entity]["data"] = $repo->findAll();
            }
        }

        // BrickIMS
        $entity = "BrickIMS";
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:'.$entity);
        $available_filtres[$entity]["label"] = $filtres[$entity];
        $available_filtres[$entity]["data"] = $repo->findFree($selectedSpecialites);
        unset($filtres[$entity]);

        // Ville
        $entity = "Ville";
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:'.$entity);
        $available_filtres[$entity]["label"] = $filtres[$entity];
        $available_filtres[$entity]["data"] = $repo->findFree($selectedSpecialites);
        unset($filtres[$entity]);

        foreach ($filtres as $entity => $label) {
            $repo = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:'.$entity);
            $available_filtres[$entity]["label"] = $label;
            $available_filtres[$entity]["data"] = $repo->findAll();
        }

        return array(
            'mode' => "Ajouter",
            'gamme' => $gamme,
            'form' => $form->createView(),
            'oRows'=>array(),
            "available_filtres"=>$available_filtres);

    }

    /**
     * @Route("/secteurhdc/edit/{id}", name="secteurhdcEdit")
     * @ParamConverter("secteur", options={"mapping": {"id": "id" }})
     * @Template("@HdcOffice/SecteurHDC/edit.html.twig")
     */
    public function editAction(Request $request, SecteurHDC $secteur)
    {
        $user = $this->getUser();

        $secteurHdcDetails = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:SecteurDetail')->findBy(
                array('secteurhdc_id'  => $secteur->getId()));

        $form = $this->createForm(new SecteurHDCType(), $secteur);
        $form->remove('gamme_id');

        $form->handleRequest($request);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();

            if ($secteurHdcDetails) {
                foreach ($secteurHdcDetails as $secteurHdcDetail) {
                    $em->remove($secteurHdcDetail);
                }
            }
            $em->flush();


            $oModel = $secteur;
            $formdata = $form->getData();
            $oModel->setName($formdata->getName());
            $oModel->setUser($user);

            if (
                (count($request->request->get("select_Ville")) && !in_array(0,$request->get("select_Ville") )) ||
                (count($request->request->get("select_BrickIMS")) && !in_array(0,$request->get("select_BrickIMS") ))
            ) {
                /* Persist the object to the database */
                $em = $this->getDoctrine()->getManager();
                $em->persist($oModel);
                $em->flush();

                $specialites = $request->request->get("select_Specialite");
                $specialiteList = [];
                if(count($specialites) == 0 || in_array('0', $specialites))
                {
                    foreach ($secteur->getGammeId()->getSpecialites() as $specialite){
                        $specialiteList[] = $specialite->getSpecialiteId();
                    }
                }
                else{
                    foreach ($specialites as $specialiteId){
                        $specialiteList[] = $this->getDoctrine()
                            ->getRepository('HdcOfficeBundle:Specialite')->find($specialiteId);;
                    }
                }

                if (count($request->request->get("select_Ville"))) {
                    $repo = $this->getDoctrine()
                        ->getRepository('HdcOfficeBundle:Ville');

                    foreach ($request->request->get("select_Ville") as $key => $ville_id) {
                        $ville = $repo->find(array("id" => $ville_id));
                        foreach ($specialiteList as $spec) {
                            $oPersistDt = new SecteurDetail();
                            $oPersistDt->setSecteurHDCId($oModel);
                            $oPersistDt->setSpecialite($spec);
                            $oPersistDt->setVilleId($ville);
                            $oPersistDt->setBrickims($ville->getBrickimsId());
                            $em->persist($oPersistDt);
                        }
                    }
                } elseif (count($request->request->get("select_BrickIMS"))) {
                    $repo = $this->getDoctrine()
                        ->getRepository('HdcOfficeBundle:BrickIMS');

                    foreach ($request->request->get("select_BrickIMS") as $key => $brickims_id) {
                        $brickims = $repo->find(array("id" => $brickims_id));
                        foreach ($specialiteList as $spec) {
                            $oPersistDt = new SecteurDetail();
                            $oPersistDt->setSecteurHDCId($oModel);
                            $oPersistDt->setSpecialite($spec);
                            $oPersistDt->setBrickims($brickims);
                            $em->persist($oPersistDt);
                        }
                    }
                }

                $em->flush();

                $this->addFlash(
                    'success',
                    'Secteur HDC modifié!'
                );

                return $this->redirect($this->generateUrl('gammelist'));
            }else{
                $this->addFlash(
                    'warning',
                    'Merci de choisir Brick IMS ou Ville!'
                );
            }
        }

        /* key = EntityName, value= label */
        $filtres = array(
            "Specialite"=>"Spécialités",
            "Region"=>"Régions",
            "Gouvernorat"=>"Gouvernorats",
            "BrickIMS"=>"Brick IMS",
            "Ville"=>"Localités"
        );

        $selectedSpecialites = array();
        foreach ($secteur->getGammeId()->getSpecialites() as $specialite)
        {
            $selectedSpecialites[] = $specialite->getSpecialiteId()->getId();
        }

        $titreId = array(9);
        if(count($selectedSpecialites)>0)
        {
            $entity = "Specialite";
            $repo = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:'.$entity);
            $available_filtres[$entity]["label"] = $filtres[$entity];
            $available_filtres[$entity]["data"] = $repo->findBy(array('id'=> $selectedSpecialites));
            unset($filtres[$entity]);
        }

        foreach ($filtres as $entity => $label) {
            if (!in_array(['BrickIMS', 'Ville'], $filtres)) {
                $repo = $this->getDoctrine()
                    ->getRepository('HdcOfficeBundle:'.$entity);
                $available_filtres[$entity]["label"] = $label;
                $available_filtres[$entity]["data"] = $repo->findAll();
            }
        }

        // BrickIMS
        $entity = "BrickIMS";
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:'.$entity);
        $available_filtres[$entity]["label"] = $filtres[$entity];
        $available_filtres[$entity]["data"] = $repo->findFree($selectedSpecialites);
        unset($filtres[$entity]);

        $SelectedAvailable_filtres = [];

        // SelectedBrickIMS
        $entity2 = "BrickIMS";
        foreach ($secteur->getDetails() as $detail){
            if($detail->getBrickims()){
                $SelectedAvailable_filtres[$entity2][$detail->getBrickims()->getId()] = 1;
            }
        }
        // SelectedVille
        $entity2 = "Ville";
        foreach ($secteur->getDetails() as $detail){
            if($detail->getVilleId()){
                $SelectedAvailable_filtres[$entity2][$detail->getVilleId()->getId()] = 1;
            }
        }

        // Ville
        $entity = "Ville";
        /** @var  Ville $repo */
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:'.$entity);
        $available_filtres[$entity]["label"] = $filtres[$entity];
        $available_filtres[$entity]["data"] = $repo->findFree(array_values($selectedSpecialites),
            !empty($SelectedAvailable_filtres['BrickIMS'])?array_keys($SelectedAvailable_filtres['BrickIMS']):null,
            false);
        unset($filtres[$entity]);

        foreach ($filtres as $entity => $label) {
            $repo = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:'.$entity);
            $available_filtres[$entity]["label"] = $label;
            $available_filtres[$entity]["data"] = $repo->findAll();
        }

        //selected Gouvernorat
        $reposGouvernorat = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:SecteurDetail')->findOneBy(
                array('secteurhdc_id'  => $secteur->getId()));
        /** @var  Gouvernorat $selectedGouvernorat */
        $selectedGouvernorat = $reposGouvernorat->getBrickims()->getGouvernoratId();

        //selected Region
        $repoRegions = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gouvernorat')->findOneBy(
                array('id'  => $selectedGouvernorat->getId()));
        $selectedRegion = $repoRegions->getRegionId();

        return array(
            'mode' => "Editer",
            'gamme' => $secteur->getGammeId(),
            'form' => $form->createView(),
            'secteurHDC' => $secteur,
            'oRows'=>array(),
            "available_filtres"=>$available_filtres,
            "selectedAvailable_filtres"=>$SelectedAvailable_filtres,
            "selectedRegion"  => $selectedRegion,
            "selectedGouvernorat"  => $selectedGouvernorat
        );
    }

    /**
     * @Route("/secteurhdc/delete/{id}", name="secteurhdcdelete")
     * @ParamConverter("secteur", options={"mapping": {"id": "id" }})
     */
    public function deleteAction(SecteurHDC $secteur)
    {
        if (!$secteur) {
            throw $this->createNotFoundException('Pas de secteur HDC');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($secteur);
        $em->flush();

        $this->addFlash(
            'success',
            'Secteur HDC supprimé!'
        );


        return $this->redirect($this->generateUrl('gammelist'));

    }
}
