<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hdc\Bundle\OfficeBundle\Entity\Visite;
use Hdc\Bundle\OfficeBundle\Form\Type\VisiteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Visite controller.
 *
 * @Route("/visite")
 */
class VisiteController extends Controller
{

    /**
     * Lists all Visite entities.
     *
     * @Route("/", name="visite")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HdcOfficeBundle:Visite')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Visite entity.
     *
     * @Route("/", name="visite_create")
     * @Method("POST")
     * @Template("HdcOfficeBundle:Visite:new.html.twig")
     */
    public function createAction(Request $request)
    {
       /* $entity = new Visite();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setDatesaisie(new \DateTime());
            $entity->setUserId($user);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('visite_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );*/
    }

    /**
     * Creates a form to create a Visite entity.
     *
     * @param Visite $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Visite $entity, $id)
    {
        $form = $this->createForm(new VisiteType(), $entity, array(
            'action' => $this->generateUrl('visite_save',array("id"=>$id)),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Visite entity.
     *
     * @Route("/new", name="visite_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        /*$entity = new Visite();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );*/
    }

    /**
     * save visite
     *
     * @Route("/save/{id}", name="visite_save")
     * @Method({"GET","POST"})
     */
    public function saveAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');
        $oPersist = $repository->findOneBy(array('id'=> $id));

        $entity = new Visite();
        $entity->setProspectId($oPersist);
        $form   = $this->createCreateForm($entity, $id);

         $user = $this->get('security.token_storage')->getToken()->getUser();

         $form->handleRequest($request);

         if ($form->isValid()) {
             $formdata = $form->getData();
             $entity->setAcces($formdata->getAcces());
             $entity->setDatevisite($formdata->getDatevisite());
             $entity->setTempsattente($formdata->getTempsattente());
             $entity->setTempsvisite($formdata->getTempsvisite());
             $entity->setConcurrent($formdata->getConcurrent());
             $entity->setRp($formdata->getRp());
             $entity->setRemarque($formdata->getRemarque());
             $entity->setDatesaisie(new \DateTime());
             $entity->setUserId($user);
             $entity->setProspectId($oPersist);

             $em = $this->getDoctrine()->getManager();
             $em->persist($entity);
             $em->flush();

             //return $this->redirect($this->generateUrl('visite_show', array('id' => $entity->getId())));
         } else var_dump($form->getErrors());

         /*return array(
             'entity' => $entity,
             'form'   => $form->createView(),
         );*/die();
    }

    /**
     * Displays a form to create a new Visite entity.
     *
     * @Route("/add/{id}", name="visite_add")
     * @Method("GET")
     */
    public function addAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');
        $oPersist = $repository->findOneBy(array('id'=> $id));

        $entity = new Visite();
        $form   = $this->createCreateForm($entity,$id );

        return $this->render('HdcOfficeBundle:Visite:add.html.twig',array(
            'entity' => $entity,
            'prospect' => $oPersist,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Visite entity.
     *
     * @Route("/{id}", name="visite_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HdcOfficeBundle:Visite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Visite entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Visite entity.
     *
     * @Route("/{id}/edit", name="visite_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HdcOfficeBundle:Visite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Visite entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Visite entity.
    *
    * @param Visite $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Visite $entity)
    {
        $form = $this->createForm(new VisiteType(), $entity, array(
            'action' => $this->generateUrl('visite_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Visite entity.
     *
     * @Route("/{id}", name="visite_update")
     * @Method("PUT")
     * @Template("HdcOfficeBundle:Visite:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HdcOfficeBundle:Visite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Visite entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('visite_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Visite entity.
     *
     * @Route("/{id}", name="visite_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HdcOfficeBundle:Visite')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Visite entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('visite'));
    }

    /**
     * Creates a form to delete a Visite entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('visite_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
