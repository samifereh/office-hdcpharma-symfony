<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Hdc\Bundle\OfficeBundle\Entity\GammeProduits;
use Hdc\Bundle\OfficeBundle\Entity\GammeSpecialites;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Gamme;
use Hdc\Bundle\OfficeBundle\Form\Type\GammeType;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class GammeController extends Controller
{
    /**
     * @Route("/gamme/", name="gammelist")
     * @Template()
     */
    public function indexAction()
    {

        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gamme');


        $objects = $repository->findAll();
        
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');
        $freeProspectCount = $repo->getFreePrivateProspect(null, true, 0 , 100 );
        return array('objects'=>$objects, "freeProspectCount" => $freeProspectCount);
    }

    /**
     * @Route("/gamme/view/{id}", name="gammeview")
     * @Template("@HdcOffice/Gamme/view.html.twig")
     */
    public function viewAction($id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gamme');


        $gamme = $repository->find($id);
        $selectedSpecialites = array();
        foreach ($gamme->getSpecialites() as $specialite)
        {
            $selectedSpecialites[] = $specialite->getSpecialiteId()->getId();
        }


        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');

        $freeProspectCount = $repo->getFreePrivateProspect($selectedSpecialites, true, 0 , 100 );

        return array('object'=>$gamme, "freeProspectCount" => $freeProspectCount, "selectedSpecialites" => $selectedSpecialites);
    }

    /**
     * @Route("/gamme/add", name="gammeadd")
     * @Template("@HdcOffice/Gamme/addedit.html.twig")
     */
    public function addAction(Request $request)
    {

        $oModel = new Gamme();

        $repoProduit = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Produit');
        $available_products = $repoProduit->findFreeForGamme();

        $repoSpecialite = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Specialite');
        $available_specialites = $repoSpecialite->findFreeForGamme();

        $repoGamme = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:GammeProduits');

        $form = $this->createForm(new GammeType(), $oModel);

        $form->handleRequest($request);

        if ($form->isValid() && count($request->request->get("selected_produit")) && count($request->request->get("selected_specialite"))) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();

            if (count($request->request->get("selected_produit")))
            {
                foreach ($request->request->get("selected_produit") as $key => $productId)
                {
                    $oPersistDt = new GammeProduits();
                    $oPersistDt->setGammeId($oModel);
                    $oPersistDt->setProduitId($repoProduit->find($productId));
                    $em->persist($oPersistDt);
                }
            }
            if (count($request->request->get("selected_specialite")))
            {
                foreach ($request->request->get("selected_specialite") as $key => $specialityId)
                {
                    $oPersistDt = new GammeSpecialites();
                    $oPersistDt->setGammeId($oModel);
                    $oPersistDt->setSpecialiteId($repoSpecialite->find($specialityId));
                    $em->persist($oPersistDt);
                }
            }

            $em->flush();

            $this->addFlash(
                'success',
                'Gamme ajoutée!'
            );

            return $this->redirect($this->generateUrl('gammelist'));
        }

        return array('mode' => "Ajouter", 'form' => $form->createView(), "available_products"=>$available_products, "available_specialites"=>$available_specialites);
    }


    /**
     * @Route("/gamme/edit/{id}", name="gammeedit")
     * @Template("@HdcOffice/Gamme/addedit.html.twig")
     */
    public function editAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $canReset = false;

        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gamme');

        $oModel = $repository->findOneBy(array('id'=> $id));

        $form = $this->createForm(new GammeType(), $oModel);

        $repoProduit = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Produit');
        $available_products = $repoProduit->findFreeForGamme();

        $repoSpecialite = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Specialite');
        $available_specialites = $repoSpecialite->findFreeForGamme();

        $repoGammeProduits = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:GammeProduits');
        $selected_products = $repoGammeProduits->findBy(array("gamme_id" => $id));

        $repoGammeSpecialites = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:GammeSpecialites');
        $selected_specialites = $repoGammeSpecialites->findBy(array("gamme_id" => $id));

        $form->handleRequest($request);

        if (
            $form->isValid() &&
            count($request->request->get("selected_produit")) &&
            count($request->request->get("selected_specialite"))) {


            $formdata = $form->getData();
            $uow = $em->getUnitOfWork();
            $uow->computeChangeSets(); // do not compute changes if inside a listener
            $changeset = $uow->getEntityChangeSet($oModel);
            if($changeset) {
               // name has changed then ok to reset
                $canReset = true;
            }
            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);

            if (count($request->request->get("selected_produit"))) {
                //$canReset = true;
                $q = $em->createQuery('delete from HdcOfficeBundle:GammeProduits tb where tb.gamme_id = ' . (int)$id);
                $numDeleted = $q->execute();
                foreach ($request->request->get("selected_produit") as $key => $prodId) {
                    $oPersistDt = new GammeProduits();
                    $oPersistDt->setGammeId($oModel);
                    $oPersistDt->setProduitId($repoProduit->find($prodId));
                    $em->persist($oPersistDt);
                }
            }

            if (count($request->request->get("selected_specialite"))) {
                //$canReset = true;
                $q = $em->createQuery('delete from HdcOfficeBundle:GammeSpecialites tb where tb.gamme_id = ' . (int)$id);
                $numDeleted = $q->execute();
                foreach ($request->request->get("selected_specialite") as $key => $specid) {
                    $oPersistDt = new GammeSpecialites();
                    $oPersistDt->setGammeId($oModel);
                    $oPersistDt->setSpecialiteId($repoSpecialite->find($specid));
                    $em->persist($oPersistDt);
                }
            }
            // if any change, then reset secteur
            if($canReset) {
                $this->getDoctrine()
                    ->getRepository('HdcOfficeBundle:SecteurHDC')->deleteByGammeId($id);
            }

            $em->flush();

            $this->addFlash(
                'success',
                'Gamme sauvegardée!'
            );

            return $this->redirect($this->generateUrl('gammelist'));
        }

        return array(
            'mode' => "Editer",
            'form' => $form->createView(),
            "available_products"=>$available_products,
            "selected_products"=>$selected_products,
            "available_specialites"=>$available_specialites,
            "selected_specialites"=>$selected_specialites
        );

    }

    /**
     * @Route("/gamme/delete/{id}", name="gammedelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gamme');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de gamme');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();

        $this->addFlash(
            'success',
            'Gamme supprimée!'
        );


        return $this->redirect($this->generateUrl('gammelist'));

    }
}
