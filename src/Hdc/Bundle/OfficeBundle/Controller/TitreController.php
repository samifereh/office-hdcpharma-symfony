<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Titre;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class TitreController extends Controller
{
    /**
     * @Route("/titre/", name="titrelist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Titre');


        $objects = $repository->findAll();
        return array('objects'=>$objects);
    }

    /**
     * @Route("/titre/add", name="titreadd")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function addAction(Request $request)
    {

        $oModel = new Titre();

        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();
            return $this->redirect($this->generateUrl('titrelist'));
        }

        return array('title'=>'Ajouter un titre', 'mode'=>'Ajouter', 'form' => $form->createView());
    }


    /**
     * @Route("/titre/edit/{id}", name="titreedit")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Titre');


        $oModel = $repository->findOneBy(array('id'=> $id));



        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();
            return $this->redirect($this->generateUrl('titrelist'));
        }

        return array('title'=>'Editer un Titre', 'mode'=>'Editer', 'form' => $form->createView());
    }

    /**
     * @Route("/titre/delete/{id}", name="titredelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Titre');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de titre');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();


        return $this->redirect($this->generateUrl('titrelist'));

    }
}
