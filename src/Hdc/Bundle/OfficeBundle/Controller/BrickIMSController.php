<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Hdc\Bundle\OfficeBundle\Entity\BrickIMS;
use Hdc\Bundle\OfficeBundle\Entity\Prospect;
use Hdc\Bundle\OfficeBundle\Entity\Ville;
use Hdc\Bundle\OfficeBundle\Form\Type\BrickIMSType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class BrickIMSController extends Controller
{
    /**
     * @Route("/brickims/", name="brickimslist")
     * @Template()
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:BrickIMS');


        $objects = $repository->findBy(
            array(),
            array('name' => 'ASC')
        );
        return array('objects'=>$objects);
    }

    /**
     * @Route("/brickims/add", name="brickimsadd")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template("@HdcOffice/BrickIMS/addedit.html.twig")
     */
    public function addAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $BrickIMS = new BrickIMS();

        $form = $this->createForm(new BrickIMSType(), $BrickIMS);

        $repoRegion = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Region');
        $available_regions = $repoRegion->findAll();

        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Ville');
        $available_cities = $repo->getAll();


        $form->handleRequest($request);

        if ( $form->isValid() && count($request->request->get("selected_ville"))) {
            $formdata = $form->getData();
            //dump($formdata);die('Hello');

            $BrickIMS->setName($formdata->getName());
            $BrickIMS->setGouvernoratId($formdata->getGouvernoratId());
            //var_dump($formdata->getName(), $formdata->getGouvernoratId());die('Hello');


            /* Persist the object to the database */
            $em->persist($BrickIMS);
            $em->flush();

            if (count($request->request->get("selected_ville")))
            {
                foreach ($request->request->get("selected_ville") as $key => $villeid)
                {
                    $oPersistDt = $repo->find($villeid);
                    $oPersistDt->setBrickimsId($BrickIMS);
                    $em->persist($oPersistDt);
                }
                $em->flush();
            }


            $this->addFlash(
                'success',
                'Brick IMS ajouté!'
            );


            return $this->redirect($this->generateUrl('brickimslist'));
        }

        return array('mode' => "Ajouter", 'form' => $form->createView(),
            "available_cities"=>$available_cities, "available_regions"=>$available_regions);
    }


    /**
     * @Route("/brickims/edit/{id}", name="brickimsedit")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template("@HdcOffice/BrickIMS/addedit.html.twig")
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:BrickIMS');

        /**
         * @var BrickIMS $BrickIMS
         */
        $BrickIMS = $repository->findOneBy(array('id'=> $id));

        $form = $this->createForm(new BrickIMSType(), $BrickIMS);

        $repoVille = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Ville');
        if($BrickIMS->getGouvernoratId()){
            $available_cities = $repoVille->findBy(
                array('gouvernorat_id'=> $BrickIMS->getGouvernoratId()->getId())
            );
        }else {

            $available_cities = $repoVille->getAll();
        }

        $repoRegion = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Region');
        $available_regions = $repoRegion->findAll();

        $repo2 = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Ville');
        $selected_cities = $repo2->findBy(array("brickims_id"=>$id));

        $form->handleRequest($request);

        if ( $form->isValid() && count($request->request->get("selected_ville"))) {
            $formdata = $form->getData();

            $BrickIMS->setName($formdata->getName());
            $BrickIMS->setGouvernoratId($formdata->getGouvernoratId());

            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($BrickIMS);
            $em->flush();

            if (count($request->request->get("selected_ville")))
            {
                $q = $em->createQuery('update HdcOfficeBundle:Ville tb SET tb.brickims_id = NULL where tb.brickims_id = '.$id);
                $numDeleted = $q->execute();
                foreach ($request->request->get("selected_ville") as $key => $villeid)
                {
                    $q = $em->createQuery('update HdcOfficeBundle:Ville tb SET tb.brickims_id = '.$id.' where tb.id = '.$villeid);
                    $numDeleted = $q->execute();

                }
            }

            $em->flush();

            $this->addFlash(
                'success',
                'Brick IMS sauvegardé!'
            );


            return $this->redirect($this->generateUrl('brickimslist'));
        }

        return array(
            'mode' => "Editer",
            'form' => $form->createView(),
            "available_cities"=>$available_cities,
            "selected_cities"=>$selected_cities,
            "available_regions"=>$available_regions
        );
    }

    /**
     * @Route("/brickims/delete/{id}", name="brickimsdelete")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:BrickIMS');


        $BrickIMS = $repository->findOneBy(array('id'=> $id));

        if (!$BrickIMS) {
            throw $this->createNotFoundException('Pas de BrickIMS trouvé');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($BrickIMS);
        $em->flush();

        $this->addFlash(
            'success',
            'Brick IMS supprimé!'
        );


        return $this->redirect($this->generateUrl('brickimslist'));

    }

    /**
     * @Route("/brickims/getbrick_by_gouvernorat/", name="brickbygouvernorat")
     * @Method({"POST"})
     */
    public function getBrickimsByGouvernoratAction(Request $request)
    {
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:BrickIMS');

        $gouvernorat_list = $request->get("gouv_id");

        if (in_array(0,$gouvernorat_list))
            $obj = $repo->findAll();
        else
            $obj = $repo->findBy(array('gouvernorat_id'=> $gouvernorat_list));

        $ret = array();
        $i=0;
        foreach ($obj as $brick) {
            $ret[$i]["id"] = $brick->getId();
            $ret[$i]["name"] = $brick->getName();
            $i++;
        }

        echo json_encode($ret);
        die();
    }

    /**
     * @Route("/brickims/getBricksByGouvernorat/", name="BricksByGouvernorat")
     * @Method({"POST"})
     */
    public function getBricksByGouvernoratAction(Request $request)
    {
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:BrickIMS');

        $gouvernorat_list = $request->get("gouv_id");

        if (in_array(0,$gouvernorat_list))
            $obj = $repo->findAll();
        else
            $obj = $repo->findBy(array('gouvernorat_id'=> $gouvernorat_list));

        $ret = array();
        $i=0;
        foreach ($obj as $brick) {
            $ret[$i]["id"] = $brick->getId();
            $ret[$i]["name"] = $brick->getName();
            $i++;
        }

        echo json_encode($ret);
        die();
    }
}
