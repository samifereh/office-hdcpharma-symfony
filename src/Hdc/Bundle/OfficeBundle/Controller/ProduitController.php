<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Hdc\Bundle\OfficeBundle\Entity\Produit;
use Hdc\Bundle\OfficeBundle\Form\Type\ProduitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */

class ProduitController extends Controller
{
    /**
     * @Route("/produit/", name="produitlist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Produit');


        $objects = $repository->findAll();
        return array('objects'=>$objects);
    }

    /**
     * @Route("/produit/add", name="produitadd")
     * @Template("@HdcOffice/Produit/addedit.html.twig")
     */
    public function addAction(Request $request)
    {

        $oModel = new Produit();

        $form = $this->createForm(new ProduitType(), $oModel);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());
            $oModel->setPalierGratuite($formdata->getPalierGratuite());
            $oModel->setPrixAchatHT($formdata->getPrixAchatHT());
            $oModel->setPrixRevientHT($formdata->getPrixRevientHT());
            $oModel->setPrixVenteHT($formdata->getPrixVenteHT());
            $oModel->setGroupeProduitId($formdata->getGroupeProduitId());
            $oModel->setTauxTVAId($formdata->getTauxTVAId());

            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();

            $this->addFlash(
                'success',
                'Enregistrement ajouté avec succès!'
            );

            return $this->redirect($this->generateUrl('produitlist'));
        }

        return array('mode' => "Ajouter", 'form' => $form->createView() );
    }


    /**
     * @Route("/produit/edit/{id}", name="produitedit")
     * @Template("@HdcOffice/Produit/addedit.html.twig")
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Produit');

        $oModel = $repository->findOneBy(array('id'=> $id));

        $form = $this->createForm(new ProduitType(), $oModel);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());
            $oModel->setPalierGratuite($formdata->getPalierGratuite());
            $oModel->setPrixAchatHT($formdata->getPrixAchatHT());
            $oModel->setPrixRevientHT($formdata->getPrixRevientHT());
            $oModel->setPrixVenteHT($formdata->getPrixVenteHT());
            $oModel->setGroupeProduitId($formdata->getGroupeProduitId());
            $oModel->setTauxTVAId($formdata->getTauxTVAId());

            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();

            $this->addFlash(
                'success',
                'Enregistrement effectué avec succès!'
            );

            return $this->redirect($this->generateUrl('produitlist'));
        }

        return array('mode' => "Editer", 'form' => $form->createView() );
    }

    /**
     * @Route("/produit/delete/{id}", name="produitdelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Produit');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de produit');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();

        $this->addFlash(
            'success',
            'Enregistrement supprimé avec succès!'
        );


        return $this->redirect($this->generateUrl('produitlist'));
    }

    /**
     * @Route("/prospect/delete/", name="deleteSelectedProducts")
     */
    public function deleteSelectedProductsAction(Request $request)
    {
        die('hello');
        if(count($_POST['checkbox'])){
            $repository = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:Produit');
            $em = $this->getDoctrine()->getManager();
            foreach($_POST['checkbox'] as $selected) {
                $prospect = $repository->find($selected);
                if (!$prospect) {
                    throw $this->createNotFoundException('Pas de prospect trouvé');
                }
                $em->remove($prospect);
            }
            $em->flush();
            $this->addFlash(
                'success',
                'produits supprimés'
            );
        }
        return $this->redirect($this->generateUrl('produitlist'));
    }
}
