<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Region;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class RegionController extends Controller
{
    /**
     * @Route("/region/", name="regionlist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Region');


        $regions = $repository->findAll();
        return array('regions'=>$regions);
    }

    /**
     * @Route("/region/add", name="regionadd")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function addAction(Request $request)
    {

        $region = new Region();

        $form = $this->createFormBuilder($region)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $region->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($region);
            $em->flush();
            return $this->redirect($this->generateUrl('regionlist'));
        }

        return array('title'=>'Ajouter une région', 'mode'=>'Ajouter', 'form' => $form->createView());
    }


    /**
     * @Route("/region/edit/{id}", name="regionedit")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Region');


        $region = $repository->findOneBy(array('id'=> $id));



        $form = $this->createFormBuilder($region)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $region->setName($formdata->getName())
            ;


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($region);
            $em->flush();
            return $this->redirect($this->generateUrl('regionlist'));
        }

        return array('title'=>'Editer une région', 'mode'=>'Editer', 'form' => $form->createView());
    }

    /**
     * @Route("/region/delete/{id}", name="regiondelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Region');


        $region = $repository->findOneBy(array('id'=> $id));

        if (!$region) {
            throw $this->createNotFoundException('Pas de région');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($region);
        $em->flush();


        return $this->redirect($this->generateUrl('regionlist'));

    }
}
