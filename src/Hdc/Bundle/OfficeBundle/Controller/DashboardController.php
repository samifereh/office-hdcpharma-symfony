<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DashboardController extends Controller
{
    /**
     * @Route("/", name="dashboard")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    public function getMessagesAction($max = 3)
    {

        $provider = $this->get('fos_message.provider');

        $threads = $provider->getInboxThreads();

        return $this->render(
            'HdcOfficeBundle:Dashboard:inbox.html.twig',
            array('threads' => $threads)
        );
    }
}
