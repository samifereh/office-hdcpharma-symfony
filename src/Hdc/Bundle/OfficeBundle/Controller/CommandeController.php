<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Hdc\Bundle\OfficeBundle\Entity\CommandeDetail;
use Hdc\Bundle\OfficeBundle\Form\Type\CommandeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Commande;

class CommandeController extends Controller
{
    /**
     * @Route("/commande/", name="commandelist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Commande');

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ( in_array( "ROLE_SUPER_ADMIN", $user->getRoles() ) )
            $oRows = $repository->findAll();
        else
            $oRows = $repository->findAll($user->getID());

        return array('oRows'=>$oRows);
    }

    /**
     * @Route("/commande/add", name="commandeadd")
     * @Template("@HdcOffice/Commande/addedit.html.twig")
     */
    public function addAction(Request $request)
    {
        $oPersist = new Commande();

        // get all products
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Produit');
        $oProducts = $repository->search();

        $userManager = $this->get('hdc.user');

        $form = $this->createForm(new CommandeType($userManager), $oPersist);
        $form->handleRequest($request);


        if ( $form->isValid() ) {
            $formdata = $form->getData();

            $oPersist->setDate($formdata->getDate());
            $oPersist->setType($formdata->getType());
            $oPersist->setReference($formdata->getReference());
            $oPersist->setProspectId($formdata->getProspectId());
            if ($formdata->getFournisseurId()) $oPersist->setFournisseurId($formdata->getFournisseurId());
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $oPersist->setUserId($user);


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oPersist);
            $em->flush();

            if (count($request->request->get("produit")))
            {
                foreach ($request->request->get("produit") as $key => $produitid)
                {

                    $aQte       = $request->request->get("quantite");
                    $aQteGratos = $request->request->get("qtegratuit");
                    $aPrdGratos = $request->request->get("produitgratuit");

                    $oPersistDt = new CommandeDetail();
                    $oPersistDt->setCommandeId($oPersist);
                    $oPersistDt->setProduitId($repository->find($produitid));
                    $oPersistDt->setQuantity($aQte[$key]);
                    $oPersistDt->setQteGratuit($aQteGratos[$key]);
                    $oPersistDt->setProduitGratuitId($repository->find($aPrdGratos[$key]));
                    $em->persist($oPersistDt);
                }
            }

            $em->flush();

            $this->addFlash(
                'notice',
                'Commande enregistré!'
            );

            // $this->addFlash is equivalent to $this->get('session')->getFlashBag()->add


            return $this->redirect($this->generateUrl('commandelist'));
        }

        return array('mode'=> "Ajouter", 'form' => $form->createView(), "oProducts"=>$oProducts);
    }


    /**
     * @Route("/commande/edit/{id}", name="commandeedit")
     * @Template("@HdcOffice/Commande/addedit.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Commande');


        $oPersist = $repository->findOneBy(array('id'=> $id));

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ( !in_array( "ROLE_SUPER_ADMIN", $user->getRoles() ) ) {
            if ($oPersist->getUserId()->getId() != $user->getId())
                throw $this->createNotFoundException('restriction');
        }

        if ( $oPersist->getstatus() > 0 ) {
            throw $this->createNotFoundException('Commande en cours de traitement ou livrée');
        }

        // get all products
        $repository2 = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Produit');
        $oProducts = $repository2->search();

        $userManager = $this->get('hdc.user');

        $form = $this->createForm(new CommandeType($userManager), $oPersist);
        $form->handleRequest($request);

        $repo2 = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:CommandeDetail');
        $details_commande = $repo2->findBy(array("commande"=>$id));


        if ( $form->isValid() ) {
            $formdata = $form->getData();

            $oPersist->setDate($formdata->getDate());
            $oPersist->setType($formdata->getType());
            $oPersist->setReference($formdata->getReference());
            $oPersist->setProspectId($formdata->getProspectId());
            if ($formdata->getFournisseurId()) $oPersist->setFournisseurId($formdata->getFournisseurId());
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $oPersist->setUserId($user);


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oPersist);
            $em->flush();

            if (count($request->request->get("produit")))
            {
                $q = $em->createQuery('delete from HdcOfficeBundle:CommandeDetail tb where tb.commande = '.$id);
                $numDeleted = $q->execute();
                foreach ($request->request->get("produit") as $key => $produitid)
                {

                    $aQte       = $request->request->get("quantite");
                    $aQteGratos = $request->request->get("qtegratuit");
                    $aPrdGratos = $request->request->get("produitgratuit");

                    $oPersistDt = new CommandeDetail();
                    $oPersistDt->setCommandeId($oPersist);
                    $oPersistDt->setProduitId($repository2->find($produitid));
                    $oPersistDt->setQuantity($aQte[$key]);
                    $oPersistDt->setQteGratuit($aQteGratos[$key]);
                    $oPersistDt->setProduitGratuitId($repository2->find($aPrdGratos[$key]));
                    $em->persist($oPersistDt);
                }
            }

            $em->flush();

            $this->addFlash(
                'notice',
                'Commande enregistré!'
            );

            // $this->addFlash is equivalent to $this->get('session')->getFlashBag()->add


            return $this->redirect($this->generateUrl('commandelist'));
        }

        return array('mode'=> "Editer", 'form' => $form->createView(), "oProducts"=>$oProducts, "details_commande"=>$details_commande);
    }


    /**
     * @Route("/commande/delete/{id}", name="commandedelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Commande');


        $oModel = $repository->findOneBy(array('id'=> $id));

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ( !in_array( "ROLE_SUPER_ADMIN", $user->getRoles() ) ) {
            if ($oModel->getUserId()->getId() != $user->getId())
                throw $this->createNotFoundException('restriction');
        }

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de commande trouvé');
        }

        if ( $oModel->getstatus() > 0 ) {
            throw $this->createNotFoundException('Commande en cours de traitement ou livrée');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();

        $this->addFlash(
            'success',
            'Commande supprimée!'
        );


        return $this->redirect($this->generateUrl('commandelist'));

    }

    /**
     * @Route("/commande/view/{id}", name="commandeview")
     * @Template()
     */
    public function viewAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Commande');

        $oModel = $repository->findOneBy(array('id'=> $id));


        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ( !in_array( "ROLE_SUPER_ADMIN", $user->getRoles() ) ) {
            if ($oModel->getUserId()->getId() != $user->getId())
                throw $this->createNotFoundException('restriction');
        }


        $oModel = $repository->findInfoOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de commande trouvé');
        }

        $repo2 = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:CommandeDetail');
        $details_commande = $repo2->findBy(array("commande"=>$id));

        return array("oCommande"=>$oModel, "details"=>$details_commande);

    }



    /**
     * @Route("/commande/getsubstituteproducts/{id}", name="commandegetsubstituteproducts")
     */
    public function getSubstituteProductsAction($id)
    {
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Produit');
        $oProduit = $repo->findOneBy(array('id'=> $id));

        $substitute = array();
        $i=0;
        if ( is_object($oProduit) ) {
            if ( $oProduit->getGroupeProduitId() ) {
                $oProduitSub = $repo->findBy(array('groupeproduit_id'=> $oProduit->getGroupeProduitId()));
                foreach($oProduitSub as $prod) {
                        $substitute[$i]['id'] = $prod->getId();
                        $substitute[$i]['name'] = $prod->getName();
                        $i++;
                }
            } else {
                $substitute[$i]['id'] = $id;
                $substitute[$i]['name'] = $oProduit->getName();
            }
            $substitute[$i]["qtefree"]=$oProduit->getPalierGratuite();
        }
        echo json_encode($substitute);
        die();

    }

    /**
     * @Route("/commande/setstatus/{id}/{status}", name="commandesetstaus")
     */
    public function setStatus($id,$status)
    {

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ( !in_array( "ROLE_SUPER_ADMIN", $user->getRoles() ) ) {
            throw $this->createNotFoundException('restriction');
        }

        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Commande');
        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de commande trouvé');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $oModel->setStatus($status);
        if($status == 2) {
            $oModel->setDateLivraison(new \DateTime("now"));
        }
        $em->persist($oModel);
        $em->flush();

        $res = array();
        echo json_encode($res);
        die();

    }
}
