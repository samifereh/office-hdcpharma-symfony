<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Genre;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class GenreController extends Controller
{
    /**
     * @Route("/genre/", name="genrelist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Genre');


        $objects = $repository->findAll();
        return array('objects'=>$objects);
    }

    /**
     * @Route("/genre/add", name="genreadd")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function addAction(Request $request)
    {

        $oModel = new Genre();

        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();
            return $this->redirect($this->generateUrl('genrelist'));
        }

        return array('title'=>'Ajouter un genre', 'mode'=>'Ajouter', 'form' => $form->createView());
    }


    /**
     * @Route("/genre/edit/{id}", name="genreedit")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Genre');


        $oModel = $repository->findOneBy(array('id'=> $id));



        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();
            return $this->redirect($this->generateUrl('genrelist'));
        }

        return array('title'=>'Editer un genre', 'mode'=>'Editer', 'form' => $form->createView());
    }

    /**
     * @Route("/genre/delete/{id}", name="genredelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Genre');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de genre');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();


        return $this->redirect($this->generateUrl('genrelist'));

    }
}
