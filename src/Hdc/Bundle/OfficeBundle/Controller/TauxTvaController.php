<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\TauxTva;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class TauxTvaController extends Controller
{
    /**
     * @Route("/tauxtva/", name="tauxtvalist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:TauxTva');


        $objects = $repository->findAll();
        return array('objects'=>$objects);
    }

    /**
     * @Route("/tauxtva/add", name="tauxtvaadd")
     * @Template()
     */
    public function addAction(Request $request)
    {

        $oModel = new TauxTva();

        $form = $this->createFormBuilder($oModel)
            ->add('tauxtva', 'integer')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setTauxTva($formdata->getTauxTva());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();

            $this->addFlash(
                'success',
                'Enregistrement ajouté avec succès!'
            );

            return $this->redirect($this->generateUrl('tauxtvalist'));
        }

        return array('form' => $form->createView());
    }


    /**
     * @Route("/tauxtva/edit/{id}", name="tauxtvaedit")
     * @Template()
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:TauxTva');


        $oModel = $repository->findOneBy(array('id'=> $id));



        $form = $this->createFormBuilder($oModel)
            ->add('tauxtva', 'integer')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setTauxTva($formdata->getTauxTva());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();

            $this->addFlash(
                'success',
                'Enregistrement effectué avec succès!'
            );

            return $this->redirect($this->generateUrl('tauxtvalist'));
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/tauxtva/delete/{id}", name="tauxtvadelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:TauxTva');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de taux');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();

        $this->addFlash(
            'success',
            'Enregistrement supprimé!'
        );

        return $this->redirect($this->generateUrl('tauxtvalist'));

    }
}
