<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Document;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Document controller.
 *
 * @Route("/document")
 */
class DocumentController extends Controller
{

    /**
     * Lists all Document entities.
     *
     * @Route("/", name="document")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $documents = $em->getRepository('HdcOfficeBundle:Document')->findAll();

        return array(
            'documents' => $documents,
        );
    }

    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/add", name="ajax_snippet_image_send")
     * @Template("@HdcOffice/Document/add.html.twig")
     */
    public function ajaxSnippetImageSendAction(Request $request)
    {
        $em = $this->container->get("doctrine.orm.default_entity_manager");

        $document = new Document();
        $media = $request->files->get('file');
        if ($media) {
            $document->setFile($media);
            $document->setPath($media->getPathName());
            $document->setName($media->getClientOriginalName());
            $document->upload();
            $em->persist($document);
            $em->flush();

            return new JsonResponse(array('success' => true));
        }

    }

    /**
     * Deletes a Document entity.
     *
     * @Route("/deleteDocument/{id}", name="deleteDocument")
     */
    public function deleteAction(Request $request , $id)
    {
        $em = $this->getDoctrine()->getManager();
        $document= $this->getDoctrine()->getRepository('HdcOfficeBundle:Document')->find($id);
        if (!$document)
        {
            throw new NotFoundHttpException("Aucun document trouvé avec l'id ".$document->getId());
        }

        $em->remove($document);
        $em->flush();

        return $this->redirect($this->generateUrl('document'));
    }


}
