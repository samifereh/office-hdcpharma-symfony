<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Gouvernorat;
use Hdc\Bundle\OfficeBundle\Entity\Region;

class GouvernoratController extends Controller
{
    /**
     * @Route("/gouvernorat/", name="gouvernoratlist")
     * @Template()
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gouvernorat');

        $gouvernorats = $repository->getAll();
        return array('gouvernorats'=>$gouvernorats);
    }

    /**
     * @Route("/gouvernorat/getgouvernorat_by_region/", name="gouvernoratbyregion")
     * @Method({"POST"})
     */
    public function getGouvernoratsByRegionAction(Request $request)
    {
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gouvernorat');

        $region_list = $request->get("region_id");

        if (in_array(0,$region_list))
            $obj = $repo->findAll();
        else
            $obj = $repo->findBy(array('region_id'=> $region_list));

        $ret = array();
        $i=0;
        foreach ($obj as $gouv) {
            $ret[$i]["id"] = $gouv->getId();
            $ret[$i]["name"] = $gouv->getName();
            $i++;
        }

        echo json_encode($ret);
        die();
    }

    /**
     * @Route("/gouvernorat/add", name="gouvernoratadd")
     * @Template()
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function addAction(Request $request)
    {

        $gouvernorat = new Gouvernorat();

        $form = $this->createFormBuilder($gouvernorat)
            ->add('name', 'text')
            ->add('region_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Region',
                'choice_label' => "name",
            ))
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $gouvernorat->setName($formdata->getName());
            $gouvernorat->setRegionId($formdata->getRegionId());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($gouvernorat);
            $em->flush();
            return $this->redirect($this->generateUrl('gouvernoratlist'));
        }

        return array('form' => $form->createView());
    }


    /**
     * @Route("/gouvernorat/edit/{id}", name="gouvernoratedit")
     * @Template()
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gouvernorat');


        $gouvernorat = $repository->findOneBy(array('id'=> $id));

        $form = $this->createFormBuilder($gouvernorat)
            ->add('name', 'text')
            ->add('region_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Region',
                'choice_label' => "name",
            ))
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $gouvernorat->setName($formdata->getName());
            $gouvernorat->setRegionId($formdata->getRegionId());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($gouvernorat);
            $em->flush();
            return $this->redirect($this->generateUrl('gouvernoratlist'));
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/gouvernorat/delete/{id}", name="gouvernoratdelete")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gouvernorat');


        $oDel = $repository->findOneBy(array('id'=> $id));

        if (!$oDel) {
            throw $this->createNotFoundException('Pas de gouvernorat trouvé');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oDel);
        $em->flush();


        return $this->redirect($this->generateUrl('gouvernoratlist'));

    }
}
