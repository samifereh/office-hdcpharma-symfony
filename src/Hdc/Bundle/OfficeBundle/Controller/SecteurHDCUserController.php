<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Hdc\Bundle\OfficeBundle\Entity\SecteurHDCDetail;
use Hdc\Bundle\OfficeBundle\Entity\SecteurHDCUser;
use Hdc\Bundle\OfficeBundle\Form\Type\SecteurHDCType;
use Hdc\Bundle\OfficeBundle\Form\Type\SecteurHDCUserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\SecteurHDC;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class SecteurHDCUserController extends Controller
{
    /**
     * @Route("/secteurhdcuser/", name="secteurhdcuserlist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:SecteurHDCUser');

        $list=array();
        $objects = $repository->findAll();
        if ( !empty($objects) ) {
            foreach ($objects as $obj) {
                $list[$obj["id"]]["user"]=$obj["lastname"]." ".$obj["firstname"];
                $list[$obj["id"]]["secteur"][]=$obj["sename"];
            }
        }

        return array('objects'=>$list);
    }

    /**
     * @Route("/secteurhdcuser/add", name="secteurhdcuseradd")
     * @Template("@HdcOffice/SecteurHDCUser/addedit.html.twig")
     */
    public function addAction(Request $request)
    {

        $oModel = new SecteurHDCUser();

        $form = $this->createForm(new SecteurHDCUserType(), $oModel);

        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:SecteurHDC');
        $available_secteurs = $repo->findAll();


        $form->handleRequest($request);

        if ( $form->isValid() && count($request->request->get("selected_secteurs"))) {
            $formdata = $form->getData();

            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();

            if (count($request->request->get("selected_secteurs")))
            {
                foreach ($request->request->get("selected_secteurs") as $key => $secteurid)
                {
                    $oPersistDt = new SecteurHDCUser();
                    $oPersistDt->setUserId($formdata->getUserId());
                    $oPersistDt->setSecteurHDCId($repo->find($secteurid));
                    $em->persist($oPersistDt);
                }
            }

            $em->flush();

            $this->addFlash(
                'success',
                'Utilisateur ajouté!'
            );


            return $this->redirect($this->generateUrl('secteurhdcuserlist'));
        }

        return array('mode' => "Ajouter", 'form' => $form->createView(), "available_secteurs"=>$available_secteurs );
    }

    /**
     * @Route("/secteurhdcuser/edit/{id}", name="secteurhdcuseredit")
     * @Template("@HdcOffice/SecteurHDCUser/addedit.html.twig")
     */
    public function editAction(Request $request, $id)
    {

        $repo2= $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:SecteurHDCUser');


        $oUser = $repo2->findOneBy(array('user_id'=> $id));

        $form = $this->createForm(new SecteurHDCUserType(), $oUser);


        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:SecteurHDC');
        $available_secteurs = $repo->findAll();

        $selected_secteurs = $repo2->findByUser(array("user_id"=>$id));

        $form->handleRequest($request);

        if ( $form->isValid() && count($request->request->get("selected_secteurs"))) {
            $formdata = $form->getData();

            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $q = $em->createQuery('delete from HdcOfficeBundle:SecteurHDCUser su where su.user_id = '.$id);
            $numDeleted = $q->execute();

            if (!$numDeleted) {
                throw $this->createNotFoundException('Erreur');
            }

            if (count($request->request->get("selected_secteurs")))
            {
                foreach ($request->request->get("selected_secteurs") as $key => $secteurid)
                {
                    $oPersistDt = new SecteurHDCUser();
                    $oPersistDt->setUserId($formdata->getUserId());
                    $oPersistDt->setSecteurHDCId($repo->find($secteurid));
                    $em->persist($oPersistDt);
                }
            }

            $em->flush();

            $this->addFlash(
                'success',
                'Utilisateur enregistré!'
            );


            return $this->redirect($this->generateUrl('secteurhdcuserlist'));
        }

        return array('mode' => "Editer", 'form' => $form->createView(), "available_secteurs"=>$available_secteurs, "selected_secteurs"=>$selected_secteurs );
    }


    /**
     * @Route("/secteurhdcuser/delete/{id}", name="secteurhdcuserdelete")
     */
    public function deleteAction($id)
    {


        $em = $this->getDoctrine()->getManager();


        $q = $em->createQuery('delete from HdcOfficeBundle:SecteurHDCUser se where se.user_id = '.$id);
        $numDeleted = $q->execute();

        if (!$numDeleted) {
            throw $this->createNotFoundException('Erreur');
        }


        $this->addFlash(
            'success',
            'Secteur HDC par utilisteur supprimé!'
        );


        return $this->redirect($this->generateUrl('secteurhdcuserlist'));

    }
}
