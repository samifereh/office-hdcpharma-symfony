<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Gouvernorat;
use Hdc\Bundle\OfficeBundle\Entity\Ville;


class VilleController extends Controller
{

    /**
     * @Route("/ville/", name="villelist")
     * @Template()
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Ville');

        $villes = $repository->getAll();
        return array('villes'=>$villes);
    }

    /**
     * @Route("/ville/add", name="villeadd")
     * @Template()
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function addAction(Request $request)
    {

        $obj = new Ville();

        $form = $this->createFormBuilder($obj)
            ->add('name', 'text')
            ->add('gouvernorat_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Gouvernorat',
                'choice_label' => "name",
            ))
            ->add('brickims_id', 'entity', array(
                'class' => 'HdcOfficeBundle:BrickIMS',
                'choice_label' => "name",
            ))
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $obj->setName($formdata->getName());
            $obj->setGouvernoratId($formdata->getGouvernoratId());
            $obj->setBrickimsId($formdata->getBrickimsId());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($obj);
            $em->flush();
            return $this->redirect($this->generateUrl('villelist'));
        }

        return array('form' => $form->createView());
    }


    /**
     * @Route("/ville/edit/{id}", name="villeedit")
     * @Template()
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function editAction(Request $request,$id)
    {

        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Ville');


        $obj = $repository->findOneBy(array('id'=> $id));

        $form = $this->createFormBuilder($obj)
            ->add('name', 'text')
            ->add('gouvernorat_id', 'entity', array(
                'class' => 'HdcOfficeBundle:Gouvernorat',
                'choice_label' => "name",
            ))
            ->add('brickims_id', 'entity', array(
                'class' => 'HdcOfficeBundle:BrickIMS',
                'choice_label' => "name",
            ))
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $obj->setName($formdata->getName());
            $obj->setGouvernoratId($formdata->getGouvernoratId());
            $obj->setBrickimsId($formdata->getBrickimsId());

            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($obj);
            $em->flush();
            return $this->redirect($this->generateUrl('villelist'));
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/ville/delete/{id}", name="villedelete")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Ville');


        $oDel = $repository->findOneBy(array('id'=> $id));

        if (!$oDel) {
            throw $this->createNotFoundException('Pas de ville trouvé');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oDel);
        $em->flush();


        return $this->redirect($this->generateUrl('villelist'));

    }

    /**
     * @Route("/ville/getvillesbybrickims/", name="villebybrickims")
     * @Method({"POST"})
     */
    public function getVillesByBrickIMS(Request $request)
    {
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Ville');



        $brick_list = $request->get("brickims_id");
        $specialiteList = $request->get("specialite_id", null);
        $titreId = $request->get("titre_id", null);


        if (in_array(0,$brick_list))
            $brick_list = null;

        $obj = $repo->findFree($specialiteList, $brick_list);

        $ret = array();
        $i=0;

        foreach ($obj as $ville) {
            $ret[$i]["id"] = $ville->getId();
            $ret[$i]["name"] = $ville->getName();
            $i++;
        }

        return new JsonResponse($ret);
    }

    /**
     * @Route("/ville/getallvillesbybrickims/", name="allvillebybrickims")
     * @Method({"POST"})
     */
    public function getAllVillesByBrickIMS(Request $request)
    {
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Ville');


        $brick_list = $request->get("brickims_id");

        if (in_array(0,$brick_list))
            $obj = $repo->findAll(array(), array('name' => 'ASC'));
        else
            $obj = $repo->findBy(array('brickims_id'=> $brick_list), array('name' => 'ASC'));

        $ret = array();
        $i=0;
        foreach ($obj as $ville) {
            $ret[$i]["id"] = $ville->getId();
            $ret[$i]["name"] = $ville->getName();
            $i++;

        }

        echo json_encode($ret);
        die();
    }
}
