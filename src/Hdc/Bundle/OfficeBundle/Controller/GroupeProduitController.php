<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\GroupeProduit;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class GroupeProduitController extends Controller
{
    /**
     * @Route("/groupeproduit/", name="groupeproduitlist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:GroupeProduit');


        $objects = $repository->findAll();
        return array('objects'=>$objects);
    }

    /**
     * @Route("/groupeproduit/add", name="groupeproduitadd")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function addAction(Request $request)
    {

        $oModel = new GroupeProduit();

        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();

            $this->addFlash(
                'success',
                'Groupe enregistré!'
            );

            return $this->redirect($this->generateUrl('groupeproduitlist'));
        }

        return array('title'=>'Ajouter un groupe de produit', 'mode'=>'Ajouter', 'form' => $form->createView());
    }


    /**
     * @Route("/groupeproduit/edit/{id}", name="groupeproduitedit")
     * @Template("@HdcOffice/Generic/addedit.html.twig")
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:GroupeProduit');


        $oModel = $repository->findOneBy(array('id'=> $id));



        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();

            $this->addFlash(
                'success',
                'Groupe enregistré!'
            );

            return $this->redirect($this->generateUrl('groupeproduitlist'));
        }

        return array('title'=>'Editer un groupe de produit', 'mode'=>'Editer', 'form' => $form->createView());
    }

    /**
     * @Route("/groupeproduit/delete/{id}", name="groupeproduitdelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:GroupeProduit');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de groupe trouvé');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();

        $this->addFlash(
            'success',
            'Groupe supprimé!'
        );

        return $this->redirect($this->generateUrl('groupeproduitlist'));

    }
}
