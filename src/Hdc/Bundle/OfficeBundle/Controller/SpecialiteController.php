<?php

namespace Hdc\Bundle\OfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Hdc\Bundle\OfficeBundle\Entity\Specialite;

/**
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */

class SpecialiteController extends Controller
{
    /**
     * @Route("/specialite/", name="specialitelist")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Specialite');


        $objects = $repository->findAll();
        return array('objects'=>$objects);
    }

    /**
     * @Route("/specialite/add", name="specialiteadd")
     * @Template()
     */
    public function addAction(Request $request)
    {

        $oModel = new Specialite();

        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('isFournisseur', 'checkbox', array("required"=>false))
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());
            $oModel->setisFournisseur($formdata->isFournisseur());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();
            return $this->redirect($this->generateUrl('specialitelist'));
        }

        return array('form' => $form->createView());
    }


    /**
     * @Route("/specialite/edit/{id}", name="specialiteedit")
     * @Template()
     */
    public function editAction(Request $request,$id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Specialite');


        $oModel = $repository->findOneBy(array('id'=> $id));



        $form = $this->createFormBuilder($oModel)
            ->add('name', 'text')
            ->add('isFournisseur', 'checkbox', array("required"=>false))
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $oModel->setName($formdata->getName());
            $oModel->setisFournisseur($formdata->isFournisseur());


            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();
            $em->persist($oModel);
            $em->flush();
            return $this->redirect($this->generateUrl('specialitelist'));
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/specialite/delete/{id}", name="specialitedelete")
     */
    public function deleteAction($id)
    {


        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Specialite');


        $oModel = $repository->findOneBy(array('id'=> $id));

        if (!$oModel) {
            throw $this->createNotFoundException('Pas de spécialité');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($oModel);
        $em->flush();


        return $this->redirect($this->generateUrl('specialitelist'));

    }
}
