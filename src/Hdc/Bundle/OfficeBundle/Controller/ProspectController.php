<?php
/**
 * @author akossentini
 */

namespace Hdc\Bundle\OfficeBundle\Controller;

use Hdc\Bundle\OfficeBundle\Entity\Prospect;
use Hdc\Bundle\OfficeBundle\Entity\Specialite;
use Hdc\Bundle\OfficeBundle\Entity\Ville;
use Hdc\Bundle\OfficeBundle\Entity\SecteurDetail;
use Hdc\Bundle\OfficeBundle\Form\Type\ProspectType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;



class ProspectController extends Controller
{
    /**
     * @Route("/prospect/", name="prospectlist")
     * @Template()
     */
    public function indexAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $brickims=array();
        $ville=array();
        $spec=array();
        if ( !in_array( "ROLE_SUPER_ADMIN", $user->getRoles() ) ) {

            $repo = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:SecteurHDCUser');
            $secteur = $repo->findOneBy(array("user_id"=>$user->getId()));

            $repo2 = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:SecteurDetail');
            if ($secteur instanceof SecteurDetail) {
                $secteurDetail = $repo2->findBy(array("secteurhdc_id" => $secteur->getSecteurHDCId()));

                foreach ($secteurDetail as $item) {
                    $spec[$item->getSpecialiteId()->getID()] = $item->getSpecialiteId()->getID();
                    $ville[$item->getVilleId()->getID()] = $item->getVilleId()->getID();
                }
            }
        }


        /* key = EntityName, value= label */
        $filtres = array(
            "Ville"=>"Localités",
            "BrickIMS"=>"Brick IMS",
            "Gouvernorat"=>"Gouvernorats",
            "Region"=>"Régions",
            "Specialite"=>"Spécialités",
            "Titre"=>"Titres",
            "Potentiel"=>'Potentiels',
            "Cursus"=>"Cursus"
        );

        $orderFilters = array(
            "Region",
            "Gouvernorat",
            "BrickIMS",
            "Ville",
            "Specialite",
            "Titre",
            "Potentiel",
            "Cursus"
        );

        foreach ($filtres as $entity => $label) {
            $repo = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:'.$entity);
            $available_filtres[$entity]["label"] = $label;


            if ($entity == "Specialite" )
            {
                if (count($spec))
                    $available_filtres[$entity]["data"] = $repo->findBy(array("id"=>$spec));
                else
                    $available_filtres[$entity]["data"] = $repo->findAll();
            }
            elseif ($entity == "Ville" )
            {
                $GouvernoratId = array();
                $brickims_id = array();
                if (count($ville)) {
                    $available_filtres[$entity]["data"] = $repo->findBy(array("id" => $ville));
                    foreach ($available_filtres[$entity]["data"] as $item) {
                        $GouvernoratId[]=  $item->getGouvernoratId();

                        $brickims_id[]=  $item->getVilleId()->getBrickimsId();
                    }

                } else
                    $available_filtres[$entity]["data"] = $repo->findBy([],array('name' => 'ASC'),100);
            }
            elseif ($entity == "BrickIMS" )
            {
                if (count($brickims_id)) {
                    $available_filtres[$entity]["data"] = $repo->findBy(array("id" => $brickims_id));
                } else
                    $available_filtres[$entity]["data"] = $repo->findAll();
            }
            elseif ($entity == "Gouvernorat" )
            {
                $region_id=array();
                if (count($GouvernoratId)) {
                    $available_filtres[$entity]["data"] = $repo->findBy(array("id" => $GouvernoratId));
                    foreach ($available_filtres[$entity]["data"] as $item) {
                        $region_id[]=  $item->getRegionId();
                    }
                } else
                    $available_filtres[$entity]["data"] = $repo->findAll();
            }
            elseif ($entity == "Region" )
            {
                if (count($region_id))
                    $available_filtres[$entity]["data"] = $repo->findBy(array("id"=>$region_id));
                else
                    $available_filtres[$entity]["data"] = $repo->findAll();
            }
            else
                $available_filtres[$entity]["data"] = $repo->findAll();
        }

        foreach ($orderFilters as $items) {
            $available_orderedfiltres[$items] = $available_filtres[$items];
        }

        $repoGamme = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Gamme');
        $gamme = $repoGamme->findAll();

        return array(
            'oRows'=>array(),
            "available_filtres" => $available_orderedfiltres,
            'gamme'=>$gamme
        );
    }

    /**
     * @Route("/prospect/add", name="prospectadd")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("@HdcOffice/Prospect/addedit.html.twig")
     */
    public function addAction(Request $request)
    {
        $oPersist = new Prospect();

        $repoRegion = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Region');
        $availableRegions = $repoRegion->findAll();

        $repoGouvernorat = $this->getDoctrine()->getRepository('HdcOfficeBundle:Gouvernorat');
        $availableGouvernorats = $repoGouvernorat->findBy(["region_id" => $availableRegions[0]]);

        $repoBrickIMS = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:BrickIMS');
        $availableBrickIMS = $repoBrickIMS->findBy(["gouvernorat_id" => $availableGouvernorats[0]]);

        $form = $this->createForm(new ProspectType(), $oPersist);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formdata = $form->getData();

            $titre = $formdata->getTitreId();

            /* Persist the object to the database */
            $em = $this->getDoctrine()->getManager();

            // check existe prospect by lastname, firstname, ville_id and specilite_id
            $repProspect = $em->getRepository('HdcOfficeBundle:Prospect');
            $exitOne = $repProspect->findOneBy([
                "lastname" => $formdata->getLastName(),
                "firstname" => $formdata->getFirstName(),
                "ville_id" => $formdata->getVilleId(),
                "specialite_id" => $formdata->getSpecialiteId()
            ]);
            if($exitOne) {
                $this->addFlash(
                    'warning',
                    'Le prospect est déjà existant!'
                );
                echo "<script> alert('Le prospect est déjà existant!')
                      </script>";
            }

            $em->persist($oPersist);
            $em->flush();

            $this->addFlash(
                'success',
                'Prospect ajouté!'
            );

            return $this->redirect($this->generateUrl('prospectlist'));
        }


        return array(
            'mode'=> "Ajouter",
            'form' => $form->createView(),
            "availableRegions"=> $availableRegions,
            "availableGouvernorats" => $availableGouvernorats,
            "availableBrickIms"     => $availableBrickIMS,
            "SelectedBrickIms"     => $availableBrickIMS[0],
        );
    }

    /**
     * @Route("/prospect/edit/{id}/", name="prospectedit")
     * @Security("has_role('ROLE_ADMIN')")
     * @ParamConverter("prospect", class="HdcOfficeBundle:Prospect")
     * @Template("@HdcOffice/Prospect/addedit.html.twig")
     */
    public function editAction(Request $request,Prospect $prospect)
    {

        $em = $this->getDoctrine()->getManager();
        //chercher la specialite liée au prospect
        $pGammeSpecialite = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:GammeSpecialites')
            ->findOneBy(["specialite_id" => $prospect->getSpecialiteId()->getId()]);

        $pBrickIms = $prospect->getVilleId()->getBrickimsId();
        $pGouvernorat = $prospect->getVilleId()->getGouvernoratId();
        $pRegion = $prospect->getVilleId()->getGouvernoratId()->getRegionId();

        $availableBrickIms = $this->getDoctrine()->getRepository('HdcOfficeBundle:BrickIMS')
            ->findBy(['gouvernorat_id' => $prospect->getVilleId()->getGouvernoratId()]);

        $repoGouvernorat = $this->getDoctrine()->getRepository('HdcOfficeBundle:Gouvernorat');
        $availableGouvernorats = $repoGouvernorat->findBy([
            "region_id" => $pGouvernorat->getRegionId()
        ]);

        $repoRegion = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Region');
        $availableRegions = $repoRegion->findAll();

        $form = $this->createForm(new ProspectType(), $prospect);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /* Persist the object to the database */
            $em->flush();
            $this->addFlash(
                'success',
                'Prospect modifié avec succès'
            );

            return $this->redirect($this->generateUrl('prospectlist'));
        }

        return array(
            'mode'                  => "Editer",
            'form'                  => $form->createView(),
            "availableRegions"      => $availableRegions,
            "SelectedRegion"               => $pRegion,
            "availableGouvernorats" => $availableGouvernorats,
            "SelectedGouvernorat"          => $pGouvernorat,
            "availableBrickIms"     => $availableBrickIms,
            "SelectedBrickIms"              => $pBrickIms,
            "SelectedGamme"                 => $pGammeSpecialite,
        );
    }

    /**
     * @Route("/prospect/delete/{id}", name="prospectdelete")
     */
    public function deleteAction($id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');

        $Prospect = $repository->findOneBy(array('id'=> $id));

        if (!$Prospect) {
            throw $this->createNotFoundException('Pas de prospect trouvé');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($Prospect);
        $em->flush();

        $this->addFlash(
            'success',
            'Prospect supprimé'
        );

        return $this->redirect($this->generateUrl('prospectlist'));

    }


    /**
     * @Route("/prospect/view/{id}", name="prospectview")
     * @Template()
     */
    public function viewAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');

        $userManager = $this->get('hdc.user');
        $list = $repository->getSecteurHDCByCurrentUser($userManager);

        $oPersist = $repository->findOneBy(array('id'=> $id));
        /*$secteurhdc = $oPersist->getSecteurHdcId();
        if( !in_array($secteurhdc->getID(), $list))
            die("not allowed");*/

        return array("oProspect"=>$oPersist);
    }

    /**
     * @Route("/prospect/getavailable_specialite_by_ville/", name="specialitebyville")
     * @Method({"POST"})
     */
    public function getSpecialiteByVilleAction(Request $request)
    {
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');

        $ville_list = $request->get("ville_id");

        if (in_array(0,$ville_list))
            $obj = $repo->findSpecialiteByVille(false);
        else
            $obj = $repo->findSpecialiteByVille(array('ville_id'=> $ville_list));

        $ret = array();
        $i=0;
        foreach ($obj as $spec) {
            $ret[$i]["id"] = $spec["specid"];
            $ret[$i]["name"] = $spec["specname"];
            $i++;
        }

        echo json_encode($ret);
        die();
    }

    /**
     * @Route("/prospect/getavailable_specialite_by_brickims/", name="specialitebybrick")
     * @Method({"POST"})
     */
    public function getSpecialiteByBrickAction(Request $request)
    {
        $repo = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');

        $brickims_list = $request->get("brickims_id");
        if ($request->get("titre_id"))
            $titre_list = $request->get("titre_id");

        if (in_array(0,$brickims_list))
            $obj = $repo->findSpecialiteByBrickIMS(false);
        else
            $obj = $repo->findSpecialiteByBrickIMS(array('brickims_id'=> $brickims_list, "titre_id"=>$titre_list));

        $ret = array();
        $i=0;
        foreach ($obj as $spec) {
            $ret[$i]["id"] = $spec["specid"];
            $ret[$i]["name"] = $spec["specname"];
            $i++;
        }

        echo json_encode($ret);
        die();
    }


    /**
     * @Route("/prospect/countprospectfilter/", name="prospectcountfiltre")
     * @Method({"POST"})
     */
    public function getCountProspectFilter(Request $request)
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');

        $critere = array();

        $user = $this->get('security.token_storage')->getToken()->getUser();


        if ( !in_array( "ROLE_SUPER_ADMIN", $user->getRoles() ) ) {

            $repo = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:SecteurHDCUser');
            $secteur = $repo->findOneBy(array("user_id"=>$user->getId()));

            $repo2 = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:SecteurDetail');
            $secteurDetail = $repo2->findBy(array("secteurhdc_id"=>$secteur->getSecteurHDCId()));

            foreach ($secteurDetail as $item) {
                $spec[$item->getSpecialiteId()->getID()]=$item->getSpecialiteId()->getID();
                $ville[$item->getVilleId()->getID()]=$item->getVilleId()->getID();
            }

            $critere['ville_id'] = array_values($ville);
            $critere['specialite_id'] = array_values($spec);

        }

        if ($request->get("specialite") && !in_array(0,$request->get("specialite") ))
            $critere['specialite_id'] = $request->get("specialite");

        if ($request->get("ville") && !in_array(0,$request->get("ville") ))
            $critere['ville_id'] = $request->get("ville");

        if ($request->get("brickims") && !in_array(0,$request->get("brickims") ))
            $critere['brickims_id'] = $request->get("brickims");

        if ($request->get("ville") && !in_array(0, $request->get("ville")))
            $critere['ville_id'] = $request->get("ville");

        if ($request->get("gouvernorat") && !in_array(0, $request->get("gouvernorat")))
            $critere['gouvernorat_id'] = $request->get("gouvernorat");

        if ($request->get("region") && !in_array(0, $request->get("region")))
            $critere['region_id'] = $request->get("region");

        if ($request->get("titre") && !in_array(0, $request->get("titre")))
            $critere['titre_id'] = $request->get("titre");

        if ($request->get("potentiel") && !in_array(0, $request->get("potentiel")))
            $critere['potentiel_id'] = $request->get("potentiel");

        if ($request->get("cursus") && !in_array(0, $request->get("cursus")))
            $critere['cursus_id'] = $request->get("cursus");


        echo $total = $repository->getCountProspect($critere);
        die();
    }

    /**
     * @Route("/prospect/countprospectfilterprivate/", name="prospectfiltreprivate")
     * @Method({"POST"})
     */
    public function getProspectFilterPrivate(Request $request)
    {
        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');

        $privateOnly = $request->get("privateOnly");

        $critere = array();

        if ($request->get("specialite") && !in_array(0, $request->get("specialite"))) {
            $critere['specialite_id']= $request->get("specialite");
        }

        if ($request->get("ville") && !in_array(0, $request->get("ville")))
            $critere['ville_id'] = $request->get("ville");

        if ($request->get("brickims") && !in_array(0, $request->get("brickims")))
            $critere['brickims_id'] = $request->get("brickims");

        $totalAll = 0;
        if(!$privateOnly)
            $totalAll = $repository->getCountProspect($critere);

        if ($request->get("titre") && !in_array(0, $request->get("titre")))
            $critere['titre_id'] = $request->get("titre");

        $totalPrivate = $repository->getCountProspect($critere);

        if($privateOnly)
            $ret = array("private"=>$totalPrivate);
        else
            $ret = array("private"=>$totalPrivate, "notprivate"=>$totalAll-$totalPrivate);

        echo json_encode($ret);
        die();
    }

    /**
     * @Route("/prospect/paging", name="prospectpaging")
     * @Method({"GET","POST"})
     */
    public function getPage(Request $request)
    {
        $orderMapping = [
            "lastname",
            "specialite_id",
            "titre_id",
            "potentiel_id",
            "cursus_id",
            "brickims_id",
            "ville_id",
            "adresse"
        ];

        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');

        $critere = array();

        $user = $this->get('security.token_storage')->getToken()->getUser();


        if ( !in_array( "ROLE_SUPER_ADMIN", $user->getRoles() ) ) {

            $repo = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:SecteurHDCUser');

            $secteur = $repo->findOneBy(array("user_id"=>$user->getId()));

            $repo2 = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:SecteurDetail');
            if($secteur) {
                $secteurDetail = $repo2->findBy(array("secteurhdc_id" => $secteur->getSecteurHDCId()));

                foreach ($secteurDetail as $item) {
                    $spec[$item->getSpecialiteId()->getID()] = $item->getSpecialiteId()->getID();
                    $ville[$item->getVilleId()->getID()] = $item->getVilleId()->getID();
                }
                $critere['ville_id'] = array_values($ville);
                $critere['specialite_id'] = array_values($spec);
            }

        }

        if ($request->get("specialite") && !in_array(0,$request->get("specialite") ))
            $critere['specialite_id'] = $request->get("specialite");

        if ($request->get("ville") && !in_array(0,$request->get("ville") ))
            $critere['ville_id'] = $request->get("ville");

        if ($request->get("brickims") && !in_array(0,$request->get("brickims") ))
            $critere['brickims_id'] = $request->get("brickims");

        if ($request->get("gouvernorat") && !in_array(0, $request->get("gouvernorat")))
            $critere['gouvernorat_id'] = $request->get("gouvernorat");

        if ($request->get("region") && !in_array(0, $request->get("region")))
            $critere['region_id'] = $request->get("region");

        if ($request->get("titre") && !in_array(0,$request->get("titre") ))
            $critere['titre_id'] = $request->get("titre");

        if ($request->get("potentiel") && !in_array(0,$request->get("potentiel") ))
            $critere['potentiel_id'] = $request->get("potentiel");

        if ($request->get("cursus") && !in_array(0,$request->get("cursus") ))
            $critere['cursus_id'] = $request->get("cursus");

        $total = $repository->getCountProspect($critere);


        $orderBy = array();
        $orderList = $request->get('order');
        foreach ($orderList as $orderItem) {
            if($orderId = $orderMapping[$orderItem["column"]]){
                $orderBy[$orderId] = $orderItem["dir"];
            }

        }
        $offset = $request->get("start");
        $length = $request->get("length");
        $oRows = $repository->getProspect($critere, $orderBy, $length, $offset);

        $ret = array(
            "draw" => $request->get("draw"),
            "page" => $offset/$length,
            "recordsTotal"=> $total,
            "recordsFiltered"=>$total
        );

        $i=0;
        foreach ($oRows as $pro) {
            $ret["data"][$i][] = "<a title=\"Afficher\" href='".$this->generateUrl('prospectview',
                    array('id' => $pro->getId()))."'>".
                $pro->getLastname() . " " . $pro->getFirstname()
                ."</a>";

            if (is_object($pro->getSpecialiteId()) )
                $ret["data"][$i][] = $pro->getSpecialiteId()->getName();
            else
                $ret["data"][$i][] = "";

            if (is_object($pro->getTitreId()) )
                $ret["data"][$i][] = $pro->getTitreId()->getName();
            else
                $ret["data"][$i][] = "";

            if (is_object($pro->getPotentielId()) )
                $ret["data"][$i][] = $pro->getPotentielId()->getName();
            else
                $ret["data"][$i][] = "";

            if (is_object($pro->getCursusId()) )
                $ret["data"][$i][] = $pro->getCursusId()->getName();
            else
                $ret["data"][$i][] = "";


            if ( $pro->getVilleId() ) {
                // gestion brick
                if ($pro->getVilleId()->getBrickIMSId())
                    $ret["data"][$i][] = $pro->getVilleId()->getBrickIMSId()->getName(); // brick
                else
                    $ret["data"][$i][] = ""; //brick

                $ret["data"][$i][] = $pro->getVilleId()->getName(); // ville
            }
            else {
                $ret["data"][$i][] = ""; //brick
                $ret["data"][$i][] = ""; //ville
            }

            $ret["data"][$i][] = $pro->getAdresse();

            $visitlink = '<a data-id="'.$pro->getId().'" data-target="#myModal" data-toggle="modal"
            class="btn btn-Basic btn-xs" title="Viste"> 
            <i class="glyphicon glyphicon-edit"></i></a>';

            $editlink = "<a href='".$this->generateUrl('prospectedit', array('id' => $pro->getId()))."'
            data-id='".$pro->getId()."' data-offset='".$offset."' onclick='return editProspect(this);'
            class=\"btn btn-info btn-xs\" title=\"modifier\"> 
            <i class=\"glyphicon glyphicon-pencil\"></i></a>&nbsp;";

            $deletelink = "<a href='".$this->generateUrl('prospectdelete', array('id' => $pro->getId()))."'
            class=\"btn btn-danger btn-xs\" title=\"supprimer\" 
            onclick=\"if(window.confirm('Voulez-vous vraiment supprimer ?'))
            {return true;}else{return false;}\">
             <i class=\"glyphicon glyphicon-trash\"></i></a>";

            $ret["data"][$i][]=$visitlink.
                (!in_array( "ROLE_SUPER_ADMIN", $user->getRoles() )?"":$editlink)
                .$deletelink;

            $i++;
        }

        echo json_encode($ret);
        die();

        echo json_encode($ret);
        die();
    }


    /**
     * @Route("/prospect/free", name="prospectfree")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function freeAction(Request $request)
    {
        $orderMapping = [
            "lastname",
            "specialite_id",
            "titre_id",
            "potentiel_id",
            "cursus_id",
            "brickims_id",
            "ville_id",
            "adresse"
        ];

        $repository = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:Prospect');

        $critere = array();

        $user = $this->get('security.token_storage')->getToken()->getUser();


        if ( !in_array( "ROLE_SUPER_ADMIN", $user->getRoles() ) ) {

            $repo = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:SecteurHDCUser');

            $secteur = $repo->findOneBy(array("user_id"=>$user->getId()));

            $repo2 = $this->getDoctrine()
                ->getRepository('HdcOfficeBundle:SecteurDetail');
            if($secteur) {
                $secteurDetail = $repo2->findBy(array("secteurhdc_id" => $secteur->getSecteurHDCId()));

                foreach ($secteurDetail as $item) {
                    $spec[$item->getSpecialiteId()->getID()] = $item->getSpecialiteId()->getID();
                    $ville[$item->getVilleId()->getID()] = $item->getVilleId()->getID();
                }

                $critere['ville_id'] = array_values($ville);
                $critere['specialite_id'] = array_values($spec);

            }

        }

        if ($request->get("specialite") && !in_array(0,$request->get("specialite") ))
            $critere['specialite_id'] = $request->get("specialite");

        if ($request->get("ville") && !in_array(0,$request->get("ville") ))
            $critere['ville_id'] = $request->get("ville");

        if ($request->get("brickims") && !in_array(0,$request->get("brickims") ))
            $critere['brickims_id'] = $request->get("brickims");


        if ($request->get("titre") && !in_array(0,$request->get("titre") ))
            $critere['titre_id'] = $request->get("titre");

        if ($request->get("potentiel") && !in_array(0,$request->get("potentiel") ))
            $critere['potentiel_id'] = $request->get("potentiel");

        if ($request->get("cursus") && !in_array(0,$request->get("cursus") ))
            $critere['cursus_id'] = $request->get("cursus");

        $total = $repository->getCountProspect($critere);


        $orderBy = array();
        $orderList = $request->get('order');
        foreach ($orderList as $orderItem) {
            if($orderId = $orderMapping[$orderItem["column"]]){
                $orderBy[$orderId] = $orderItem["dir"];
            }

        }

        $oRows = $repository->getProspect($critere, $orderBy, $request->get("length"), $request->get("start"));


        $ret = array("draw"=> $request->get("draw"),  "recordsTotal"=> $total,  "recordsFiltered"=>$total );

        $i=0;
        foreach ($oRows as $pro) {
            $ret["data"][$i][] = "<a title=\"Afficher\" href='".$this->generateUrl('prospectview',
                    array('id' => $pro->getId()))."'>".
                $pro->getLastname() . " " . $pro->getFirstname()
                ."</a>";

            $ret["data"][$i][] = $pro->getSpecialiteId()->getName();

            if ($pro->getTitreId() )
                $ret["data"][$i][] = $pro->getTitreId()->getName();
            else
                $ret["data"][$i][] = "";

            if (is_object($pro->getPotentielId()) )
                $ret["data"][$i][] = $pro->getPotentielId()->getName();
            else
                $ret["data"][$i][] = "";

            if (is_object($pro->getCursusId() ))
                $ret["data"][$i][] = $pro->getCursusId()->getName();
            else
                $ret["data"][$i][] = "";


            if ( $pro->getVilleId() ) {
                // gestion brick
                if ($pro->getVilleId()->getBrickIMSId())
                    $ret["data"][$i][] = $pro->getVilleId()->getBrickIMSId()->getName(); // brick
                else
                    $ret["data"][$i][] = ""; //brick

                $ret["data"][$i][] = $pro->getVilleId()->getName(); // ville
            }
            else {
                $ret["data"][$i][] = ""; //brick
                $ret["data"][$i][] = ""; //ville
            }

            $ret["data"][$i][] = $pro->getAdresse();

            $visitlink = '<a data-id="'.$pro->getId().'" data-target="#myModal" data-toggle="modal"
            class="btn btn-Basic btn-xs" title="Viste"> 
            <i class="glyphicon glyphicon-edit"></i></a>';

            $editlink = "<a href='".$this->generateUrl('prospectedit', array('id' => $pro->getId()))."'
            class=\"btn btn-info btn-xs\"  title=\"modifier\">
            <i class=\"glyphicon glyphicon-pencil\"></i></a>&nbsp;";

            $deletelink = "<a href='".$this->generateUrl('prospectdelete', array('id' => $pro->getId()))."'
            class=\"btn btn-danger btn-xs\" title=\"supprimer\" 
            onclick=\"if(window.confirm('Voulez-vous vraiment supprimer ?'))
            {return true;}else{return false;}\">
             <i class=\"glyphicon glyphicon-trash\"></i></a>";

            $ret["data"][$i][]=$visitlink.
                (!in_array( "ROLE_SUPER_ADMIN", $user->getRoles() )?"":$editlink)
                .$deletelink;

            $i++;
        }

        echo json_encode($ret);
        die();

        echo json_encode($ret);
        die();
    }

    /**
     * @Route("/prospect/getGammeBySpeciality/{id}", name="GammeBySpeciality")
     * @ParamConverter("speciality", class="HdcOfficeBundle:Specialite")
     * @Method({"GET"})
     * @return JsonResponse
     */
    public function getGammeBySpecialitytAction(Request $request,Specialite $speciality)
    {
        //chercher la specialite liée au prospect
        $repoGammeSpecialite = $this->getDoctrine()
            ->getRepository('HdcOfficeBundle:GammeSpecialites');
        $gammeSpecialite = $repoGammeSpecialite->findOneBy(["specialite_id" => $speciality]);
        $return = [];
        if($gammeSpecialite) {
            $return = [
                "id" => $gammeSpecialite->getGammeId()->getId(),
                "name" => $gammeSpecialite->getGammeId()->getName()
            ];

        }

        return new JsonResponse($return);
    }

}
