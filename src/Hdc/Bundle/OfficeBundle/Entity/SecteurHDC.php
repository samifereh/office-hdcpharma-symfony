<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\SecteurHDC")
 * @ORM\Table(name="secteurhdc")
 */
class SecteurHDC
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Gamme")
     * @ORM\JoinColumn(name="gamme_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $gamme_id;

    /**
     * @ORM\OneToMany(targetEntity="SecteurDetail", mappedBy="secteurhdc_id", cascade={"remove"})
     */
    protected $details;

    /**
     * @ORM\OneToMany(targetEntity="Prospect", mappedBy="secteurhdc_id")
     *  @ORM\JoinColumn(name="prospect_id", referencedColumnName="id", nullable=true, onDelete="SET NULL"))
     */
    protected $prospect;

    /**
     * @ORM\ManyToOne(targetEntity="User", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SecteurHDC
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Gamme
     *
     * @param int $gammeID
     * @return SecteurHDC
     */
    public function setGammeId(\Hdc\Bundle\OfficeBundle\Entity\Gamme $gammeID = null)
    {
        $this->gamme_id = $gammeID;

        return $this;
    }

    /**
     * Get gamme_id
     *
     * @return Gamme
     */
    public function getGammeId()
    {
        return $this->gamme_id;
    }

    /**
     * Get details
     *
     * @return array
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}

