<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\GammeProduits")
 * @ORM\Table(name="gamme_produits")
 */
class GammeProduits
{

    protected $produits;

    public function __construct()
    {
        $this->produits = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Gamme", inversedBy="produits")
     * @ORM\JoinColumn(name="gamme_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $gamme_id;

    /**
     * @ORM\ManyToOne(targetEntity="Produit")
     * @ORM\JoinColumn(name="produit_id", referencedColumnName="id")
     */
    protected $produit_id;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gamme_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Gamme $gammeID
     * @return GammeProduits
     */
    public function setGammeId(\Hdc\Bundle\OfficeBundle\Entity\Gamme $gammeID = null)
    {
        $this->gamme_id = $gammeID;

        return $this;
    }

    /**
     * Get gamme_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\Gamme
     */
    public function getGammeId()
    {
        return $this->gamme_id;
    }

    /**
     * Set produit_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Produit $produitId
     * @return GammeProduits
     */
    public function setProduitId(\Hdc\Bundle\OfficeBundle\Entity\Produit $produitId = null)
    {
        $this->produit_id = $produitId;

        return $this;
    }

    /**
     * Get produit_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\Produit
     */
    public function getProduitId()
    {
        return $this->produit_id;
    }

    public function getProduits()
    {
        return $this->produits;
    }
}
