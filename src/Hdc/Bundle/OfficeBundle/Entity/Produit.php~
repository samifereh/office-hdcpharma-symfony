<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\Produit")
 * @ORM\Table(name="produit")
 */
class Produit
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $palier_gratuite;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $prix_achatht;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $prix_revientht;

    /**
     * @ORM\Column(type="float")
     */
    protected $prix_venteht;

    /**
     * @ORM\ManyToOne(targetEntity="GroupeProduit", inversedBy="produit")
     * @ORM\JoinColumn(name="groupeproduit_id", referencedColumnName="id", nullable=true)
     */
    protected $groupeproduit_id;

    /**
     * @ORM\ManyToOne(targetEntity="TauxTva", inversedBy="produit")
     * @ORM\JoinColumn(name="tauxtva_id", referencedColumnName="id")
     */
    protected $tauxtva_id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Produit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param int $palier_gratuite
     * @return Produit
     */
    public function setPalierGratuite($palier_gratuite)
    {
        $this->palier_gratuite = $palier_gratuite;

        return $this;
    }

    /**
     * Get palier gratuite
     *
     * @return int
     */
    public function getPalierGratuite()
    {
        return $this->palier_gratuite;
    }

    /**
     * Set prix_achatht
     *
     * @param float $prix_achatht
     * @return Produit
     */
    public function setPrixAchatHT($prix_achatht)
    {
        $this->prix_achatht = $prix_achatht;

        return $this;
    }

    /**
     * Get prix_achatht
     *
     * @return float
     */
    public function getPrixAchatHT()
    {
        return $this->prix_achatht;
    }

    /**
     * Set prix_revientht
     *
     * @param float $prix_revientht
     * @return Produit
     */
    public function setPrixRevientHT($prix_revientht)
    {
        $this->prix_revientht = $prix_revientht;

        return $this;
    }

    /**
     * Get prix_revientht
     *
     * @return float
     */
    public function getPrixRevientHT()
    {
        return $this->prix_revientht;
    }

    /**
     * Set prix_venteht
     *
     * @param float $prix_revientht
     * @return Produit
     */
    public function setPrixVenteHT($prix_venteht)
    {
        $this->prix_venteht = $prix_venteht;

        return $this;
    }

    /**
     * Get prix_venteht
     *
     * @return float
     */
    public function getPrixVenteHT()
    {
        return $this->prix_venteht;
    }

    /**
     * Set groupeproduit_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\GroupeProduit $groupeproduit_id
     * @return Produit
     */
    public function setGroupeProduitId(\Hdc\Bundle\OfficeBundle\Entity\GroupeProduit $groupeproduit_id = null)
    {
        $this->groupeproduit_id = $groupeproduit_id;

        return $this;
    }

    /**
     * Get groupeproduit_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\GroupeProduit
     */
    public function getGroupeProduitId()
    {
        return $this->groupeproduit_id;
    }

    /**
     * Set tauxtva_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\TauxTva $tauxtva_id
     * @return Produit
     */
    public function setTauxTVAId(\Hdc\Bundle\OfficeBundle\Entity\TauxTva $tauxtva_id = null)
    {
        $this->tauxtva_id = $tauxtva_id;

        return $this;
    }

    /**
     * Get tauxtva_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\TauxTva
     */
    public function getTauxTVAId()
    {
        return $this->tauxtva_id;
    }
}
