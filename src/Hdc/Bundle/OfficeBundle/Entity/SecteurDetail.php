<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="secteur_detail")
 */
class SecteurDetail
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="SecteurHDC", fetch="EAGER")
     * @ORM\JoinColumn(name="secteurhdc_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $secteurhdc_id;

    /**
     * @ORM\ManyToOne(targetEntity="Specialite", fetch="EAGER")
     * @ORM\JoinColumn(name="specialite_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $specialite;

    /**
     * @ORM\ManyToOne(targetEntity="Ville", fetch="EAGER")
     * @ORM\JoinColumn(name="ville_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $ville_id;

    /**
     * @ORM\ManyToOne(targetEntity="BrickIMS", fetch="EAGER")
     * @ORM\JoinColumn(name="brickims_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $brickims;

    /**
     * @return BrickIMS
     */
    public function getBrickims()
    {
        return $this->brickims;
    }

    /**
     * @param mixed $brickims
     */
    public function setBrickims($brickims)
    {
        $this->brickims = $brickims;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set secteurhdc_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\SecteurHDC $secteurhdc_id
     * @return SecteurDetail
     */
    public function setSecteurHDCId(\Hdc\Bundle\OfficeBundle\Entity\SecteurHDC $secteurhdc = null)
    {
        $this->secteurhdc_id = $secteurhdc;

        return $this;
    }

    /**
     * Get secteurhdc_id
     *
     * @return int
     */
    public function getSecteurHDCId()
    {
        return $this->secteurhdc_id;
    }

    /**
     * Set Ville
     *
     * @param int $villeId
     * @return SecteurDetail
     */
    public function setVilleId(\Hdc\Bundle\OfficeBundle\Entity\Ville $villeId = null)
    {
        $this->ville_id = $villeId;

        return $this;
    }

    /**
     * Get ville_id
     *
     * @return ville
     */
    public function getVilleId()
    {
        return $this->ville_id;
    }

    /**
     * set specialite
     *
     * @return SecteurDetail
     */
    public function setSpecialite(\Hdc\Bundle\OfficeBundle\Entity\Specialite $specialiteId = null)
    {
        $this->specialite = $specialiteId;

        return $this;
    }

    /**
     * Get specialite
     *
     * @return specialite
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

}
