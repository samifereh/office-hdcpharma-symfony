<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\BonGratuiteDetail")
 * @ORM\Table(name="gratuite_detail")
 */

class BonGratuiteDetail
{

    protected $produits;

    public function __construct()
    {
        $this->produits = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="BonGratuite")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id", onDelete="CASCADE")
     */
    protected $bongratuite;

    /**
     * @ORM\ManyToOne(targetEntity="Produit")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $produit;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param int $qty
     * @return BonGratuiteDetail
     */
    public function setQuantity($qty)
    {
        $this->quantity = $qty;

        return $this;
    }

    /**
     * Get Quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set bongratuite_id
     *
     * @param Hdc\Bundle\OfficeBundle\Entity\BonGratuite $bonId
     * @return BonGratuite
     */
    public function setBonId(\Hdc\Bundle\OfficeBundle\Entity\BonGratuite $bon)
    {
        $this->bongratuite = $bon;

        return $this;
    }

    /**
     * Get bongratuite_id
     *
     * @return bongratuite_id
     */
    public function getBonId()
    {
        return $this->bongratuite_id;
    }

    /**
     * Set produit_id
     *
     * @param int $produitId
     * @return BonGratuite
     */
    public function setProduitId(\Hdc\Bundle\OfficeBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit_id
     *
     * @return produit_id
     */
    public function getProduitId()
    {
        return $this->produit_id;
    }

    public function getProduits()
    {
        return $this->produits;
    }
}
