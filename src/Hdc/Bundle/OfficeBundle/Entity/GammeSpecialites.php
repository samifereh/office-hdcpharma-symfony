<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\GammeSpecialites")
 * @ORM\Table(name="gamme_specialites")
 */
class GammeSpecialites
{

    protected $specialites;

    public function __construct()
    {
        $this->specialites = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Gamme", inversedBy="specialites")
     * @ORM\JoinColumn(name="gamme_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $gamme_id;

    /**
     * @ORM\ManyToOne(targetEntity="Specialite")
     * @ORM\JoinColumn(name="specialite_id", referencedColumnName="id")
     */
    protected $specialite_id;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gamme_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Gamme $gammeID
     * @return GammeProduits
     */
    public function setGammeId(\Hdc\Bundle\OfficeBundle\Entity\Gamme $gammeID = null)
    {
        $this->gamme_id = $gammeID;

        return $this;
    }

    /**
     * Get gamme_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\Gamme
     */
    public function getGammeId()
    {
        return $this->gamme_id;
    }

    /**
     * Set specialite_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Specialite $specialiteId
     * @return GammeSpecialites
     */
    public function setSpecialiteId(\Hdc\Bundle\OfficeBundle\Entity\Specialite $specialiteId = null)
    {
        $this->specialite_id = $specialiteId;

        return $this;
    }

    /**
     * Get specialite_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\Specialite
     */
    public function getSpecialiteId()
    {
        return $this->specialite_id;
    }

    public function getSpecialites()
    {
        return $this->specialites;
    }
}
