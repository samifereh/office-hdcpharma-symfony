<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\Visite")
 * @ORM\Table(name="visite")
 */

class Visite
{

    protected $prospects;

    public function __construct()
    {
        $this->prospects = new ArrayCollection();
        $this->datevisite = new \DateTime();
        $this->datesaisie = new \DateTime();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $datevisite;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $datesaisie;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="visite")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="Prospect", inversedBy="visite")
     * @ORM\JoinColumn(name="prospect_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $prospect_id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $tempsattente;

    /**
     * @ORM\Column(type="integer")
     */
    protected $tempsvisite;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $remarque;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $rp;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $concurrent;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $acces;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datevisite
     *
     * @param \DateTime $datevisite
     * @return Visite
     */
    public function setDatevisite($datevisite)
    {
        $this->datevisite = $datevisite;

        return $this;
    }

    /**
     * Get datevisite
     *
     * @return \DateTime 
     */
    public function getDatevisite()
    {
        return $this->datevisite;
    }

    /**
     * Set datesaisie
     *
     * @param \DateTime $datesaisie
     * @return Visite
     */
    public function setDatesaisie($datesaisie)
    {
        $this->datesaisie = $datesaisie;

        return $this;
    }

    /**
     * Get datesaisie
     *
     * @return \DateTime 
     */
    public function getDatesaisie()
    {
        return $this->datesaisie;
    }

    /**
     * Set tempsattente
     *
     * @param integer $tempsattente
     * @return Visite
     */
    public function setTempsattente($tempsattente)
    {
        $this->tempsattente = $tempsattente;

        return $this;
    }

    /**
     * Get tempsattente
     *
     * @return integer 
     */
    public function getTempsattente()
    {
        return $this->tempsattente;
    }

    /**
     * Set tempsvisite
     *
     * @param integer $tempsvisite
     * @return Visite
     */
    public function setTempsvisite($tempsvisite)
    {
        $this->tempsvisite = $tempsvisite;

        return $this;
    }

    /**
     * Get tempsvisite
     *
     * @return integer 
     */
    public function getTempsvisite()
    {
        return $this->tempsvisite;
    }

    /**
     * Set remarque
     *
     * @param string $remarque
     * @return Visite
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string 
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set rp
     *
     * @param string $rp
     * @return Visite
     */
    public function setRp($rp)
    {
        $this->rp = $rp;

        return $this;
    }

    /**
     * Get rp
     *
     * @return string 
     */
    public function getRp()
    {
        return $this->rp;
    }

    /**
     * Set concurrent
     *
     * @param string $concurrent
     * @return Visite
     */
    public function setConcurrent($concurrent)
    {
        $this->concurrent = $concurrent;

        return $this;
    }

    /**
     * Get concurrent
     *
     * @return string 
     */
    public function getConcurrent()
    {
        return $this->concurrent;
    }

    /**
     * Set acces
     *
     * @param integer $acces
     * @return Visite
     */
    public function setAcces($acces)
    {
        $this->acces = $acces;

        return $this;
    }

    /**
     * Get acces
     *
     * @return integer 
     */
    public function getAcces()
    {
        return $this->acces;
    }

    /**
     * Set user_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\User $userId
     * @return Visite
     */
    public function setUserId(\Hdc\Bundle\OfficeBundle\Entity\User $userId = null)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\User 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set prospect_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Prospect $prospectId
     * @return Visite
     */
    public function setProspectId(\Hdc\Bundle\OfficeBundle\Entity\Prospect $prospectId = null)
    {
        $this->prospect_id = $prospectId;

        return $this;
    }

    /**
     * Get prospect_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\Prospect 
     */
    public function getProspectId()
    {
        return $this->prospect_id;
    }
}
