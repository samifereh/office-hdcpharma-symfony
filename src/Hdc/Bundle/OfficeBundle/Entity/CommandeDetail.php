<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\CommandeDetail")
 * @ORM\Table(name="commande_detail")
 */

class CommandeDetail
{

    protected $produits;

    public function __construct()
    {
        $this->produits = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Commande")
     * @ORM\JoinColumn(name="commande_id", nullable=false, referencedColumnName="id", onDelete="CASCADE")
     */
    protected $commande;

    /**
     * @ORM\ManyToOne(targetEntity="Produit")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $produit;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="Produit")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $produitgratuit;

    /**
     * @ORM\Column(type="integer")
     */
    protected $qtegratuit;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param int $qty
     * @return CommandeDetail
     */
    public function setQuantity($qty)
    {
        $this->quantity = $qty;

        return $this;
    }

    /**
     * Get Quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }


    /**
     * Set qtegratuit
     *
     * @param int $qty
     * @return CommandeDetail
     */
    public function setQteGratuit($qty)
    {
        $this->qtegratuit = $qty;

        return $this;
    }

    /**
     * Get qtegratuit
     *
     * @return int
     */
    public function getQteGratuit()
    {
        return $this->qtegratuit;
    }


    /**
     * Set commande
     *
     * @param Hdc\Bundle\OfficeBundle\Entity\Commande $commande
     * @return Commande
     */
    public function setCommandeId(\Hdc\Bundle\OfficeBundle\Entity\Commande $commande)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande_id
     *
     * @return commande_id
     */
    public function getCommandeId()
    {
        return $this->commande_id;
    }

    /**
     * Set produit_id
     *
     * @param int $produitId
     * @return Commande
     */
    public function setProduitId(\Hdc\Bundle\OfficeBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit_id
     *
     * @return produit_id
     */
    public function getProduitId()
    {
        return $this->produit_id;
    }

    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * Set produitgratuit
     *
     * @param int $produitgratuit
     * @return Commande
     */
    public function setProduitGratuitId(\Hdc\Bundle\OfficeBundle\Entity\Produit $produitgratuit = null)
    {
        $this->produitgratuit = $produitgratuit;

        return $this;
    }

    /**
     * Get produitgratuit
     *
     * @return produitgratuit_id
     */
    public function getProduitGratuitId()
    {
        return $this->produitgratuit;
    }

}
