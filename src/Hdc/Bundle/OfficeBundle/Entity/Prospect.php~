<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\Prospect")
 * @ORM\Table(name="prospect")
 */

class Prospect
{

    protected $specialites;
    protected $titres;
    protected $villes;
    protected $genres;
    protected $cursus;
    protected $potentiel;
    protected $secteurhdc;
    protected $brickims;
    protected $completename;

    public function __construct()
    {
        $this->specialites  = new ArrayCollection();
        $this->titres       = new ArrayCollection();
        $this->villes       = new ArrayCollection();
        $this->genres       = new ArrayCollection();
        $this->cursus       = new ArrayCollection();
        $this->potentiel    = new ArrayCollection();
        $this->brickims     = new ArrayCollection();
        $this->secteurhdc     = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $adresse;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $tel1;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $tel2;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $remarque;

    /**
     * @ORM\ManyToOne(targetEntity="Specialite", inversedBy="prospect")
     * @ORM\JoinColumn(name="specialite_id", referencedColumnName="id")
     */
    protected $specialite_id;

    /**
     * @ORM\ManyToOne(targetEntity="Titre", inversedBy="prospect")
     * @ORM\JoinColumn(name="titre_id", referencedColumnName="id")
     */
    protected $titre_id;

    /**
     * @ORM\ManyToOne(targetEntity="Ville", inversedBy="prospect")
     * @ORM\JoinColumn(name="ville_id", referencedColumnName="id", nullable=true)
     */
    protected $ville_id;

    /**
     * @ORM\ManyToOne(targetEntity="Genre", inversedBy="prospect")
     * @ORM\JoinColumn(name="genre_id", referencedColumnName="id")
     */
    protected $genre_id;

    /**
     * @ORM\ManyToOne(targetEntity="Cursus", inversedBy="prospect")
     * @ORM\JoinColumn(name="cursus_id", referencedColumnName="id")
     */
    protected $cursus_id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $lat;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $lng;

    /**
     * @ORM\ManyToOne(targetEntity="Potentiel", inversedBy="prospect")
     * @ORM\JoinColumn(name="potentiel_id", referencedColumnName="id", nullable=true)
     */
    protected $potentiel_id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $rps;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $structure;

    /**
     * @ORM\ManyToOne(targetEntity="BrickIMS", inversedBy="prospect")
     * @ORM\JoinColumn(name="brickims_id", referencedColumnName="id")
     */
    protected $brickims_id;

    /**
     * @ORM\ManyToOne(targetEntity="SecteurHDC", inversedBy="prospect")
     * @ORM\JoinColumn(name="secteurhdc_id", referencedColumnName="id", nullable=true)
     */
    protected $secteurhdc_id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set last name
     *
     * @param string $lastname
     * @return Prospect
     */
    public function setLastName($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get last name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastname;
    }

    /**
 * Set first name
 *
 * @param string $firstname
 * @return Prospect
 */
    public function setFirstName($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get first name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstname;
    }

    /**
 * Set adresse
 *
 * @param text $adresse
 * @return Prospect
 */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return text
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set Tel1
     *
     * @param string $tel1
     * @return Prospect
     */
    public function setTelephone1($tel1)
    {
        $this->tel1 = $tel1;

        return $this;
    }

    /**
     * Get Tel1
     *
     * @return string
     */
    public function getTelephone1()
    {
        return $this->tel1;
    }

    /**
     * Set Tel2
     *
     * @param string $tel2
     * @return Prospect
     */
    public function setTelephone2($tel2)
    {
        $this->tel2 = $tel2;

        return $this;
    }

    /**
     * Get Tel2
     *
     * @return string
     */
    public function getTelephone2()
    {
        return $this->tel2;
    }

    /**
     * Set Email
     * @param string $email
     * @return Prospect
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get Email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set remarque
     * @param text $remarque
     * @return Prospect
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return text
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set specialite_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Specialite $specialiteId
     * @return Prospect
     */
    public function setSpecialiteId(\Hdc\Bundle\OfficeBundle\Entity\Specialite $specialiteId = null)
    {
        $this->specialite_id = $specialiteId;

        return $this;
    }

    /**
     * Get specialite_id
     */
    public function getSpecialiteId()
    {
        return $this->specialite_id;
    }

    public function getSpecialites()
    {
        return $this->specialites;
    }

    /**
     * Set titre_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Titre $titreId
     * @return Prospect
     */
    public function setTitreId(\Hdc\Bundle\OfficeBundle\Entity\Titre $titreId = null)
    {
        $this->titre_id = $titreId;

        return $this;
    }

    /**
     * Get titre_id
     */
    public function getTitreId()
    {
        return $this->titre_id;
    }

    public function getTitres()
    {
        return $this->titres;
    }

    /**
     * Set ville_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Ville $villeId
     * @return Prospect
     */
    public function setVilleId(\Hdc\Bundle\OfficeBundle\Entity\Ville $villeId = null)
    {
        $this->ville_id = $villeId;

        return $this;
    }

    /**
     * Get ville_id
     */
    public function getVilleId()
    {
        return $this->ville_id;
    }

    public function getVilles()
    {
        return $this->villes;
    }

    /**
     * Set cursus object
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Cursus $cursus
     * @return Prospect
     */
    public function setCursusId(\Hdc\Bundle\OfficeBundle\Entity\Cursus $cursus = null)
    {
        $this->cursus_id = $cursus;

        return $this;
    }

    /**
     * Get cursus_id
     */
    public function getCursusId()
    {
        return $this->cursus_id;
    }

    public function getCursus()
    {
        return $this->cursus;
    }

    /**
     * Set genre object
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Genre $genre
     * @return Prospect
     */
    public function setGenreId(\Hdc\Bundle\OfficeBundle\Entity\Genre $genre = null)
    {
        $this->genre_id = $genre;

        return $this;
    }

    /**
     * Get genre_id
     */
    public function getGenreId()
    {
        return $this->genre_id;
    }

    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * Set Latitude
     *
     * @param string $lat
     * @return Prospect
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get Latitude
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set Longitude
     *
     * @param string $lng
     * @return Prospect
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get Longitude
     *
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }


    /**
     * Set RPs
     *
     * @param text $rps
     * @return Prospect
     */
    public function setRPs($rps)
    {
        $this->rps = $rps;

        return $this;
    }

    /**
     * Get RPs
     *
     * @return text
     */
    public function getRPs()
    {
        return $this->rps;
    }

    /**
     * Set Structure
     *
     * @param text $structure
     * @return Prospect
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;

        return $this;
    }

    /**
     * Get Structure
     *
     * @return text
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * Set brickims object
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\BrickIMS $brickims
     * @return Prospect
     */
    public function setBrickIMSId(\Hdc\Bundle\OfficeBundle\Entity\BrickIMS $brickims = null)
    {
        $this->brickims_id = $brickims;

        return $this;
    }

    /**
     * Get brickims_id
     */
    public function getBrickIMSId()
    {
        return $this->brickims_id;
    }

    public function getBrickIMS()
    {
        return $this->brickims;
    }

    /**
     * Set secteurhdc object
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\SecteurHDC $secteurhdc
     * @return Prospect
     */
    public function setSecteurHdcId(\Hdc\Bundle\OfficeBundle\Entity\SecteurHDC $secteurhdc = null)
    {
        $this->secteurhdc_id = $secteurhdc;

        return $this;
    }

    /**
     * Get secteurhdc_id
     */
    public function getSecteurHdcId()
    {
        return $this->secteurhdc_id;
    }

    public function getSecteurHDC()
    {
        return $this->secteurhdc;
    }

    public function getCompleteName() {
        return $this->lastname.' '.$this->firstname;
    }

    /**
     * Set potentiel object
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Potentiel $potentiel
     * @return Prospect
     */
    public function setPotentielId(\Hdc\Bundle\OfficeBundle\Entity\Potentiel $potentiel = null)
    {
        $this->potentiel_id = $potentiel;

        return $this;
    }

    /**
     * Get cursus_id
     */
    public function getPotentielId()
    {
        return $this->potentiel_id;
    }

    public function getPotentiel()
    {
        return $this->potentiel;
    }

}
