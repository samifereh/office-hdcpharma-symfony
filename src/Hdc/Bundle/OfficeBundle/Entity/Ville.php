<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\Ville")
 * @ORM\Table(name="ville")
 */
class Ville
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Gouvernorat", inversedBy="ville")
     * @ORM\JoinColumn(name="gouvernorat_id", referencedColumnName="id")
     */
    protected $gouvernorat_id;

    /**
     * @ORM\ManyToOne(targetEntity="BrickIMS", inversedBy="ville")
     * @ORM\JoinColumn(name="brickims_id", referencedColumnName="id")
     */
    protected $brickims_id;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Ville
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set gouvernorat_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Gouvernorat $gouvernoratId
     * @return Ville
     */
    public function setGouvernoratId(\Hdc\Bundle\OfficeBundle\Entity\Gouvernorat $gouvernoratId = null)
    {
        $this->gouvernorat_id = $gouvernoratId;

        return $this;
    }

    /**
     * Get gouvernorat_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\Gouvernorat 
     */
    public function getGouvernoratId()
    {
        return $this->gouvernorat_id;
    }

    /**
     * Set brickims_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\BrickIMS $BrickIMSeId
     * @return Ville
     */
    public function setBrickimsId(\Hdc\Bundle\OfficeBundle\Entity\BrickIMS $BrickIMSeId = null)
    {
        $this->brickims_id = $BrickIMSeId;

        return $this;
    }

    /**
     * Get brickims_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\BrickIMS
     */
    public function getBrickimsId()
    {
        return $this->brickims_id;
    }
}
