<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tauxtva")
 */
class TauxTva
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="float")
     */
    protected $tauxtva;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taux_tva
     *
     * @param string $name
     * @return TauxTVA
     */
    public function setTauxTva($taux_tva)
    {
        $this->tauxtva = $taux_tva;

        return $this;
    }

    /**
     * Get $taux_tva
     *
     * @return float
     */
    public function getTauxTva()
    {
        return $this->tauxtva;
    }
}
