<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\Gouvernorat")
 * @ORM\Table(name="gouvernorat")
 */

class Gouvernorat
{

    protected $regions;

    public function __construct()
    {
        $this->regions = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="gouvernorat")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    protected $region_id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Gouvernorat
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set region_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Region $regionId
     * @return Gouvernorat
     */
    public function setRegionId(\Hdc\Bundle\OfficeBundle\Entity\Region $regionId = null)
    {
        $this->region_id = $regionId;

        return $this;
    }

    /**
     * Get region_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\Region 
     */
    public function getRegionId()
    {
        return $this->region_id;
    }

    public function getRegions()
    {
        return $this->regions;
    }
}
