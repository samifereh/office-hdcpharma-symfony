<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\Commande")
 * @ORM\Table(name="commande")
 */

class Commande
{

    protected $prospects;
    protected $fournisseurs;

    public function __construct()
    {
        $this->prospects = new ArrayCollection();
        $this->fournisseurs = new ArrayCollection();
        $this->date = new \DateTime();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $reference;

    /**
     * @ORM\Column(type="smallint", length=1)
     */
    protected $type;

    /**
     * @ORM\Column(type="smallint", length=1, options={"default" = 0})
     */
    protected $status;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="commande")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="Prospect", inversedBy="commande")
     * @ORM\JoinColumn(name="prospect_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $prospect_id;

    /**
     * @ORM\ManyToOne(targetEntity="Prospect", inversedBy="commande")
     * @ORM\JoinColumn(name="fournisseur_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $fournisseur_id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $datelivraison;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type (-1 sortie / 1 entree)
     *
     * @param string $type
     * @return Commande
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set reference
     *
     * @param string $ref
     * @return Commande
     */
    public function setReference($ref)
    {
        $this->reference = $ref;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set date
     *
     * @param date $date
     * @return Commande
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Commande
     */
    public function setUserId(\Hdc\Bundle\OfficeBundle\Entity\User $user = null)
    {
        $this->user_id = $user;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return userID
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set prospect_id
     *
     * @param Prospect $prospect
     * @return Commande
     */
    public function setProspectId(\Hdc\Bundle\OfficeBundle\Entity\Prospect $prospect)
    {
        $this->prospect_id = $prospect;

        return $this;
    }

    /**
     * Get prospect_id
     *
     * @return prospectID
     */
    public function getProspectId()
    {
        return $this->prospect_id;
    }

    public function getProspects()
    {
        return $this->prospects;
    }

    /**
     * Set fournisseur_id
     *
     * @param Prospect $fournisseurID
     * @return Commande
     */
    public function setFournisseurId(\Hdc\Bundle\OfficeBundle\Entity\Prospect $fournisseurID)
    {
        $this->fournisseur_id = $fournisseurID;

        return $this;
    }

    /**
     * Get fournisseur_id
     *
     * @return fournisseurID
     */
    public function getFournisseurId()
    {
        return $this->fournisseur_id;
    }

    public function getFournisseurs()
    {
        return $this->fournisseurs;
    }

    /**
     * Set status (0 en attente, 1 en cours de traitement, 2 livrée)
     *
     * @param int $status
     * @return Commande
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getstatus()
    {
        return $this->status;
    }

    /**
     * Set datelivraison
     *
     * @param date $datelivraison
     * @return Commande
     */
    public function setDateLivraison($date)
    {
        $this->datelivraison = $date;

        return $this;
    }

    /**
     * Get datelivraison
     *
     * @return date
     */
    public function getDateLivraison()
    {
        return $this->datelivraison;
    }

}
