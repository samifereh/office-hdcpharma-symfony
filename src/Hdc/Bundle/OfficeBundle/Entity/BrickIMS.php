<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\BrickIMS")
 * @ORM\Table(name="brickims")
 */
class BrickIMS
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $code;

    /**
     * @ORM\ManyToOne(targetEntity="Gouvernorat", inversedBy="ville" )
     * @ORM\JoinColumn(name="gouvernorat_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $gouvernorat_id;

    /**
     * @var ArrayCollection $villes
     * @ORM\OneToMany(targetEntity="Ville", mappedBy="brickims_id")
     */
    protected $villes;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BrickIMS
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set gouvernorat_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\Gouvernorat $gouvernoratId
     * @return Ville
     */
    public function setGouvernoratId(\Hdc\Bundle\OfficeBundle\Entity\Gouvernorat $gouvernoratId = null)
    {
        $this->gouvernorat_id = $gouvernoratId;

        return $this;
    }

    /**
     * Get gouvernorat_id
     *
     * @return \Hdc\Bundle\OfficeBundle\Entity\Gouvernorat
     */
    public function getGouvernoratId()
    {
        return $this->gouvernorat_id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return BrickIMS
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return ArrayCollection
     */
    public function getVilles()
    {
        return $this->villes;
    }

    /**
     * @param ArrayCollection $villes
     */
    public function setVilles($villes)
    {
        $this->villes = $villes;
    }


}
