<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\Specialite")
 * @ORM\Table(name="specialite")
 */
class Specialite
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isfournisseur;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Specialite
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isFournisseur
     *
     * @param boolean $isFournisseur
     * @return Specialite
     */
    public function setisFournisseur($isFournisseur)
    {
        $this->isfournisseur = $isFournisseur;

        return $this;
    }

    /**
     * Get isFournisseur
     *
     * @return boolean
     */
    public function isFournisseur()
    {
        return $this->isfournisseur;
    }
}
