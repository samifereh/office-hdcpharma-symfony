<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="gamme")
 */
class Gamme
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @var ArrayCollection $specialites
     * @ORM\OneToMany(targetEntity="GammeSpecialites", mappedBy="gamme_id")
     */
    protected $specialites;

    /**
     * @var ArrayCollection $produits
     * @ORM\OneToMany(targetEntity="GammeProduits", mappedBy="gamme_id")
     */
    protected $produits;

    /**
     * @var ArrayCollection $secteurs
     * @ORM\OneToMany(targetEntity="SecteurHDC", mappedBy="gamme_id")
     */
    protected $secteurs;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param text $name
     * @return Cursus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return text
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get specialites
     *
     * @return array
     */
    public function getSpecialites()
    {
        return $this->specialites;
    }

    /**
     * Get produits
     *
     * @return array
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * Get secteurs
     *
     * @return array
     */
    public function getSecteurs()
    {
        return $this->secteurs;
    }
}
