<?php

namespace Hdc\Bundle\OfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Hdc\Bundle\OfficeBundle\Repository\SecteurHDCUser")
 * @ORM\Table(name="secteurhdc_user")
 */
class SecteurHDCUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="SecteurHDC", fetch="EAGER")
     * @ORM\JoinColumn(name="secteurhdc_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $secteurhdc_id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user_id;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set secteurhdc_id
     *
     * @param \Hdc\Bundle\OfficeBundle\Entity\SecteurHDC $secteurhdc_id
     * @return SecteurHDCUser
     */
    public function setSecteurHDCId(\Hdc\Bundle\OfficeBundle\Entity\SecteurHDC $secteurhdc = null)
    {
        $this->secteurhdc_id = $secteurhdc;

        return $this;
    }

    /**
     * Get secteurhdc_id
     *
     * @return int
     */
    public function getSecteurHDCId()
    {
        return $this->secteurhdc_id;
    }

    /**
     * Set user
     *
     * @param int $userId
     * @return SecteurHDCUser
     */
    public function setUserId(\Hdc\Bundle\OfficeBundle\Entity\User $user = null)
    {
        $this->user_id = $user;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return userID
     */
    public function getUserId()
    {
        return $this->user_id;
    }
}
